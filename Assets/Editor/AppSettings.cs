﻿using UnityEditor;
using UnityEngine;

public class AppSettings : EditorWindow
{
	// Add menu item named "My Window" to the Window menu
	[MenuItem("Game/App Settings")]
	public static void ShowWindow()
	{
		//Show existing window instance. If one doesn't exist, make one.
		EditorWindow window = EditorWindow.GetWindow(typeof(AppSettings));
		window.Show();
	}
	
	void OnGUI()
	{
		GUILayout.Label("Settings", EditorStyles.boldLabel);
		
		PlayerSettings.bundleVersion = EditorGUILayout.TextField("Bundle Version", PlayerSettings.bundleVersion);
	}
}