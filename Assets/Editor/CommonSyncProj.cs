﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;

[InitializeOnLoad]
public class CommonSyncProj
{	
	static CommonSyncProj()
	{
		if (Application.isEditor)
		{
			//CommonSync.UpdateItemData();
		
			EditorApplication.update += CommonSync.SyncCommonFiles;
		}
	}
}
