﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class CommonSyncWin : EditorWindow
{
	private Vector2 mScrollPos = Vector3.zero;
	private bool mIsCopyingFromEx = false;
	private bool mIsCopyingToEx = false;
	private bool mIsDeleting = false;
	
	[MenuItem ("Window/CommonSync")]
	static void Init()
	{
		CommonSync.UpdateItemData();
	
		//
		CommonSyncWin window = (CommonSyncWin)EditorWindow.GetWindow (typeof(CommonSyncWin));
		window.Show();
	}

	void OnGUI()
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label("CommonSync", EditorStyles.boldLabel);
		GUILayout.EndHorizontal();
		
		EditorGUILayout.Space();
		
		if (mIsCopyingFromEx)
		{
			GUIShowCopyFromExCheck();
			return;
		}
		else if (mIsCopyingToEx)
		{
			GUIShowCopyToExCheck();
			return;
		}
		else if (mIsDeleting)
		{
			GUIShowDelete();
			return;
		}
		
		CommonSync.ExternalPath = EditorGUILayout.TextField("External Path", CommonSync.ExternalPath);
		
		//
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Local Path", GUILayout.Width(146));
		GUILayout.Label(CommonSync.LocalPath);
		GUILayout.EndHorizontal();
		
		EditorGUILayout.Space();
		
		//
		
		GUILayout.BeginHorizontal();
		
			//Left
			GUILayout.BeginVertical();
			
				GUILayout.Label("Files\t(Revision " + CommonSync.UpdatedRevisionNum + ")");
			
				//Scroll
				mScrollPos = EditorGUILayout.BeginScrollView(mScrollPos);
				
				GUIShowFileData(CommonSync.RootItems);
				
				EditorGUILayout.EndScrollView();
		
			GUILayout.EndVertical();

			//Right
			GUILayout.BeginVertical(GUILayout.Width(200));
			
			GUILayout.Label("");
			
			GUIShowControls();
			
			GUILayout.EndVertical();
		
		GUILayout.EndHorizontal();		
		
		//
		
		EditorGUILayout.Space();
		
		if (GUI.changed)
		{
		//	Debug.Log("Change");
		}
	}
	
	protected void GUIShowFileData(CommonSync.ItemData item)
	{
		if (item == null)
			return;
		if (item.childItems == null)
			return;
		
		for (int i = 0; i < item.childItems.Count; i++)
		{
			CommonSync.ItemData childItem = item.childItems[i];
			
			EditorGUILayout.BeginHorizontal();
			
			EditorGUILayout.LabelField("", GUILayout.Width(12.0f * childItem.DirLevel));

			bool wasSelected = childItem.Selected;
			bool selected = EditorGUILayout.Toggle(childItem.Selected, GUILayout.Width(12.0f));
			
			//Update selectrion tree
			if (wasSelected && !selected)
				CommonSync.SetSelectItemData(childItem, false);
			else if (!wasSelected && selected)
				CommonSync.SetSelectItemData(childItem, true);

			if (childItem.FileInfo != null)
				EditorGUILayout.LabelField(childItem.FileInfo.Name);
							
			if (childItem.DirInfo != null)
				EditorGUILayout.LabelField(childItem.DirInfo.Name + "/");
			
			EditorGUILayout.EndHorizontal();
			
			//if (childItem.Selected)
			{
				//Recursive
				GUIShowFileData(item.childItems[i]);
			}
		}		
	}
	
	protected void GUIShowControls()
	{
		if (GUILayout.Button("Update List"))
			CommonSync.UpdateItemData();
		if (GUILayout.Button("Select All"))
			CommonSync.SetSelectItemData(CommonSync.RootItems, true);
		if (GUILayout.Button("Select None"))
			CommonSync.SetSelectItemData(CommonSync.RootItems, false);
		
		EditorGUILayout.Space();
		
		if (GUILayout.Button("Copy From External"))
			mIsCopyingFromEx = true;
//		if (GUILayout.Button("Copy To External"))
//			mIsCopyingToEx = true;

		EditorGUILayout.Space();
		
		if (GUILayout.Button("Delete"))
			mIsDeleting = true;
	}
	
	protected void GUIShowCopyFromExCheck()
	{
		GUILayout.Label("Copy External -> Local");
		GUILayout.Label("This will copy selected files from the external folder to the local folder. " +
		                "Local files will be overwritten. Are you sure?", EditorStyles.wordWrappedLabel);
		
		GUILayout.BeginHorizontal();
		
		if (GUILayout.Button("OK"))
		{
			CommonSync.CopyItemDataFromExternal(CommonSync.RootItems);
			
			AssetDatabase.Refresh();
			
			mIsCopyingFromEx = false;
		}
		if (GUILayout.Button("Cancel"))
		{
			mIsCopyingFromEx = false;
		}
		
		GUILayout.EndHorizontal();
	}
	
	protected void GUIShowCopyToExCheck()
	{
		GUILayout.Label("Copy Local -> External");
		GUILayout.Label("This will copy selected files from the local folder to the external folder. " +
		                "External files will be overwritten. Are you sure?", EditorStyles.wordWrappedLabel);
		
		GUILayout.BeginHorizontal();
		
		if (GUILayout.Button("OK"))
		{
			CommonSync.CopyItemDataToExternal(CommonSync.RootItems);
			
			AssetDatabase.Refresh();
			
			mIsCopyingToEx = false;
		}
		if (GUILayout.Button("Cancel"))
		{
			mIsCopyingToEx = false;
		}
		
		GUILayout.EndHorizontal();
	}

	protected void GUIShowDelete()
	{
		GUILayout.Label("Deleting");
		GUILayout.Label("This will delete selected files in both the external and local folders. Are you sure?", EditorStyles.wordWrappedLabel);
		
		GUILayout.BeginHorizontal();
		
		if (GUILayout.Button("OK"))
		{
			CommonSync.DeleteFilesFromItemData(CommonSync.RootItems);
			CommonSync.DeleteDirectoriesFromItemData(CommonSync.RootItems);
			CommonSync.UpdateItemData();
			
			AssetDatabase.Refresh();
			
			mIsDeleting = false;
		}
		if (GUILayout.Button("Cancel"))
		{
			mIsDeleting = false;
		}
		
		GUILayout.EndHorizontal();
	}
}
