﻿//inspired by http://answers.unity3d.com/questions/45186/can-i-auto-run-a-script-when-editor-launches-or-a.html

using UnityEngine;
using UnityEditor;
using System.IO;

[InitializeOnLoad]
public class VersionIncrementor
{
	static VersionIncrementor()   
	{
		//If you want the scene to be fully loaded before your startup operation,
		//for example to be able to use Object.FindObjectsOfType, you can defer your
		//logic until the first editor update, like this:
		EditorApplication.update += RunOnce;
	}
	
	static void RunOnce()   
	{
		EditorApplication.update -= RunOnce;
		ReadVersionAndIncrement();
	}
	
	static void ReadVersionAndIncrement()    
	{
		string versionText = PlayerSettings.bundleVersion;

		versionText = versionText.Trim(); //clean up whitespace if necessary
		string[] lines = versionText.Split('.');
		
		if (lines.Length < 3)
		{
			Debug.LogWarning("Version text is not big enough!");
			return;
		}

		int MajorVersion = int.Parse(lines[0]);
		int MinorVersion = int.Parse(lines[1]);
		int SubMinorVersion = int.Parse(lines[2]) + 1; //increment here
		
		versionText = MajorVersion.ToString("0") + "." +
						MinorVersion.ToString("0") + "." +
						SubMinorVersion.ToString("0000");

		PlayerSettings.bundleVersion = versionText;
	}
}