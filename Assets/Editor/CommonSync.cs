﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class CommonSync
{
	[System.Serializable]
	public class ItemData
	{
		public bool Selected = false;
		public FileInfo FileInfo = null;
		public DirectoryInfo DirInfo = null;
		public int DirLevel = 0;
		
		public string LocalDest = "";
		
		public ItemData parentItem = null;
		
		[SerializeField]
		public List<ItemData> childItems = null;
	}
	
	protected static string mRevFileName = "RevisionNum.txt";
	
	[SerializeField]
	protected static string mExternalPath = "/Users/Raz/SVN/Home/Unity/Common/";
	public static string ExternalPath				{set{mExternalPath=value;} get{return mExternalPath;}}
	
	protected static string mLocalPath = Application.dataPath + "/Common/";
	public static string LocalPath					{set{mLocalPath=value;} get{return mLocalPath;}}

	protected static ItemData mRootItems = new ItemData();
	public static ItemData RootItems				{get{return mRootItems;}}
	
	protected static int mUpdatedRevisionNum = 0;
	public static int UpdatedRevisionNum			{get{return mUpdatedRevisionNum;}}
	
	public static void UpdateItemData()
	{
		//Clear
		mRootItems = new ItemData();
		
		if (mExternalPath == "")
		{
			Debug.LogWarning("External path is not set!");
			return;
		}
		
		//Get file info in folder recursively
		FillItemData(mExternalPath, mLocalPath, ref mRootItems, 0);
		
		mUpdatedRevisionNum = GetExternalRevision();
	}
	
	protected static void FillItemData(string sourcePath, string destinationPath,
										ref ItemData item, int dirLevel)
	{
		DirectoryInfo dirInfo = new DirectoryInfo(sourcePath);
		if (dirInfo == null)
			return;

		//Create child array
		
		item.childItems = new List<ItemData>();
		
		//Get files
		
		FileInfo[] fileInfos = dirInfo.GetFiles();
		for (int i = 0; i < fileInfos.Length; i++)
		{
			if (CanCopyFile(fileInfos[i]))
			{
				ItemData childItem = new ItemData();
				childItem.FileInfo = fileInfos[i];
				childItem.DirLevel = dirLevel;
				
				childItem.LocalDest = destinationPath;

				childItem.parentItem = item;				
				item.childItems.Add(childItem);
			}
		}
		
		//Get directories
		
		DirectoryInfo[] dirInfos = dirInfo.GetDirectories();
		for (int i = 0; i < dirInfos.Length; i++)
		{
			ItemData childItem = new ItemData();
			childItem.DirInfo = dirInfos[i];
			childItem.DirLevel = dirLevel;
			
			childItem.LocalDest = destinationPath;

			childItem.parentItem = item;
			item.childItems.Add(childItem);
			
			//Recursive
			FillItemData(dirInfos[i].FullName, Path.Combine(destinationPath, dirInfos[i].Name),
							ref childItem, dirLevel+1);
		}
	}
	
	protected static bool CanCopyFile(FileInfo info)
	{
		//Ignore these
		if (info.Name == mRevFileName)
			return false;
		if (info.Extension == ".meta")
			return false;
		if (info.Extension == ".DS_Store")
			return false;
		return true;
	}
	
	//
	
	public static void SetSelectItemData(ItemData item, bool select)
	{
		item.Selected = select;
		
		SetSelectItemDataParent(item, select);
		SetSelectItemDataChildren(item, select);
	}

	public static void SetSelectItemDataParent(ItemData item, bool select)
	{
		if (item.parentItem != null)
		{
			if (select)
				item.parentItem.Selected = true;
			else
			{
				//Find if any of it's sibling is selected
				bool allSiblingsUnselected = true;
				if (item.parentItem.childItems != null)
				{
					for (int i = 0; i < item.parentItem.childItems.Count; i++)
					{
						if (item.parentItem.childItems[i].Selected)
						{
							allSiblingsUnselected = false;
							break;
						}
					}
				}
				
				//Don't unselect if there's still a sibling selected
				if (allSiblingsUnselected)
					item.parentItem.Selected = false;
			}
			
			SetSelectItemDataParent(item.parentItem, select);
		}
	}
	
	public static void SetSelectItemDataChildren(ItemData item, bool select)
	{
		if (item.childItems != null)
		{
			for (int i = 0; i < item.childItems.Count; i++)
			{
				ItemData childItem = item.childItems[i];
				childItem.Selected = select;
				
				SetSelectItemData(childItem, select);
			}
		}
	}
	
	//
	
	public static void CopyItemDataFromExternal(ItemData item)
	{
		if (item.childItems == null)
			return;
			
		for (int i = 0; i < item.childItems.Count; i++)
		{
			ItemData childItem = item.childItems[i];
			if (childItem.Selected)
			{
				if (childItem.FileInfo != null)
				{
					string destinationPath = childItem.LocalDest;

					if (Directory.Exists(destinationPath) == false)
						Directory.CreateDirectory(destinationPath);
						
					CopyFileInfo(childItem.FileInfo, destinationPath);
				}
			}
			
			CopyItemDataFromExternal(childItem);
		}
	}
	
	public static void CopyItemDataToExternal(ItemData item)
	{
		if (item.childItems == null)
			return;
		
		for (int i = 0; i < item.childItems.Count; i++)
		{
			ItemData childItem = item.childItems[i];
			if (childItem.Selected)
			{
				if (childItem.FileInfo != null)
				{
					string filePath = Path.Combine(childItem.LocalDest, childItem.FileInfo.Name);
					FileInfo fileInfo = new FileInfo(filePath);
					string destinationPath = childItem.FileInfo.DirectoryName;
		
					//Debug.Log("COPY " + filePath + " " + destinationPath);
														
					if (Directory.Exists(destinationPath) == false)
						Directory.CreateDirectory(destinationPath);
					
					CopyFileInfo(fileInfo, destinationPath);
				}
			}
			
			CopyItemDataToExternal(childItem);
		}
	}
	
	public static void DeleteFilesFromItemData(ItemData item)
	{
		if (item.childItems == null)
			return;
		
		for (int i = item.childItems.Count-1; i >= 0 ; i--)
		{
			ItemData childItem = item.childItems[i];
			
			//Do children first
			DeleteFilesFromItemData(childItem);

			if (childItem.Selected)
			{
				if (childItem.FileInfo != null)
				{
					string localPath = Path.Combine(childItem.LocalDest, childItem.FileInfo.Name);
					string externalPath = childItem.FileInfo.FullName;
					
					//Debug.Log("DELETE " + localPath + " " + externalPath);
					
					if (File.Exists(localPath))
						File.Delete(localPath);
					if (File.Exists(externalPath))
						File.Delete(externalPath);
						
					//Remove
					item.childItems.Remove(childItem);
				}
			}
		}
	}

	public static void DeleteDirectoriesFromItemData(ItemData item)
	{
		if (item.childItems == null)
			return;
		
		for (int i = item.childItems.Count-1; i >= 0 ; i--)
		{
			ItemData childItem = item.childItems[i];
			
			//Do children first
			DeleteDirectoriesFromItemData(childItem);

			if (childItem.Selected)
			{
				if (childItem.DirInfo != null)
				{
					//Find if there are no files
		
					if (childItem.childItems.Count == 0)
					{
						string localPath = Path.Combine(childItem.LocalDest, childItem.DirInfo.Name);
						string externalPath = childItem.DirInfo.FullName;
						
						//Debug.Log("DELETE " + localPath + " " + externalPath);
						
						if (Directory.Exists(localPath))
							Directory.Delete(localPath, true);
						if (Directory.Exists(externalPath))
							Directory.Delete(externalPath, true);

						//Remove
						item.childItems.Remove(childItem);
					}
				}
			}
		}
	}

	//
	
	public static void SyncCommonFiles()
	{
		EditorApplication.update -= SyncCommonFiles;
		
		if (mExternalPath == "")
		{
			Debug.LogWarning("External path is not set!");
			return;
		}
		
		//
		
		int externalRev = GetExternalRevision();
		int localRev = GetLocalRevision();
		
		//Debug.Log("ex=" + externalRev + " loc=" + localRev);

		if (externalRev > localRev)
		{
			localRev = externalRev;
			
			//Newer common files
			if (CopyDirectory(mExternalPath, mLocalPath))
				Debug.Log("Copied from external");

			AssetDatabase.Refresh();
		}
		else
		{
			//Increase rev counter
			localRev++;
			
			string content = "" + localRev;
			WriteTextFile(mExternalPath + mRevFileName, content);
			
			//
			if (CopyDirectory(mLocalPath, mExternalPath))
				Debug.Log("Copied to external");
				
			UpdateItemData();
		}

		PlayerPrefs.SetInt("localRev", localRev);
	}
	
	public static int GetExternalRevision()
	{
		string content = ReadTextFile(mExternalPath + mRevFileName);
		return Convert.ToInt32(content);
	}

	public static int GetLocalRevision()
	{
		return PlayerPrefs.GetInt("localRev");
	}
	
	protected static bool CopyDirectory(string sourcePath, string destinationPath)
	{
		bool copied = false;
		
		if (Directory.Exists(destinationPath) == false)
			Directory.CreateDirectory(destinationPath);
	
		//
		
		DirectoryInfo dirInfo = new DirectoryInfo(sourcePath);
		FileInfo[] files = dirInfo.GetFiles();
		
		foreach (FileInfo tempFile in files)
		{
			if (CopyFileInfo(tempFile, destinationPath))
				copied = true;
		}
			
		//
		
		DirectoryInfo[] dirInfos = dirInfo.GetDirectories();

		foreach (DirectoryInfo tempDir in dirInfos)
		{
			if (CopyDirectory(Path.Combine(sourcePath, tempDir.Name), Path.Combine(destinationPath, tempDir.Name)))
				copied = true;
		}
		
		return copied;
	}
	
	protected static bool CopyFileInfo(FileInfo fileInfo, string destinationPath)
	{
		if (CommonSync.CanCopyFile(fileInfo))
		{
			string destFilePath = Path.Combine(destinationPath, fileInfo.Name);
			
			if (File.Exists(destFilePath))
			{
				//Compare write time, only overwrite if source is newer
				
				long source = fileInfo.LastWriteTime.Ticks;
				long dest = File.GetLastWriteTime(destFilePath).Ticks;
				
				DateTime lastWriteTime = fileInfo.LastWriteTime;
				
				//Debug.Log(fileInfo.LastWriteTime.ToString() + " " + File.GetLastWriteTime(destFilePath).ToString());
				
				if (source > dest)
				{
					Debug.Log("Copied " + fileInfo.Name);
					
					fileInfo.CopyTo(destFilePath, true);
					
					//Keep old time
					File.SetLastWriteTime(destFilePath, lastWriteTime);
					
					return true;
				}
			}
			else
			{
				fileInfo.CopyTo(destFilePath, true);
				
				return true;
			}
		}
		
		return false;
	}
	
	protected static string ReadTextFile(string sFileName)
	{
		//Check to see if the filename specified exists, if not try adding '.txt', otherwise fail
		string sFileNameFound = "";
		if (File.Exists(sFileName))
		{
			sFileNameFound = sFileName; //file found
		}
		else if (File.Exists(sFileName + ".txt"))
		{
			sFileNameFound = sFileName + ".txt";
		}
		else
		{
			Debug.Log("Could not find file '" + sFileName + "'.");
			return null;
		}
		
		StreamReader sr;
		try
		{
			sr = new StreamReader(sFileNameFound);
		}
		catch (System.Exception e)
		{
			Debug.LogWarning("Something went wrong with read.  " + e.Message);
			return null;
		}
		
		string fileContents = sr.ReadToEnd();
		sr.Close();
		
		return fileContents;
	}
	
	protected static void WriteTextFile(string sFilePathAndName, string sTextContents)
	{
		StreamWriter sw = new StreamWriter(sFilePathAndName);
		sw.WriteLine(sTextContents);
		sw.Flush();
		sw.Close();
	}
}
