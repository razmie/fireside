﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DebugData
{
	//LevelManager
	public bool DebugText = false;

	//Player
	public bool DebugTextPlayer = false;
	public bool DontSpawnPlayer = false;
	public bool InvinciblePlayer = false;
	
	//Enemies
	public bool DebugTextEnemy = false;
	public bool DontSpawnEnemy = false;
	public bool InvincibleEnemies = false;
	
	//Misc
	public bool HideUI = false;
	
	public void Reset()
	{
		DebugText = false;

		DebugTextPlayer = false;
		DontSpawnPlayer = false;
		InvinciblePlayer = false;

		DebugTextEnemy = false;
		DontSpawnEnemy = false;
		InvincibleEnemies = false;

		HideUI = false;
	}
}

[ExecuteInEditMode]
public class DBG : LevelObject
{
	[SerializeField][HideInInspector]
	protected bool mCanDebug = false;
	public bool CanDebug			{get{return mCanDebug;} set{mCanDebug = value;}}
	
	[SerializeField][HideInInspector]
	public DebugData data = new DebugData();
	public static DebugData Data	{get{return Instance.data;}}
	
	protected static DBG mInstance = null;
	public static DBG Instance
	{
		get
		{
			if(mInstance == null)
			{
				mInstance = GameObject.FindObjectOfType<DBG>();
				DontDestroyOnLoad(mInstance.gameObject);
			}
			return mInstance;
		}
	}

	//For UI states
	[HideInInspector] public bool LevelManagerFoldout = false;
	[HideInInspector] public bool RoomFoldout = false;
	[HideInInspector] public bool PlayerFoldout = false;
	[HideInInspector] public bool EnemiesFoldout = false;
	[HideInInspector] public bool MiscFoldout = false;
	
	protected override void Awake()
	{
		base.Awake();
		
		if(mInstance == null)
		{
			mInstance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			if(this != mInstance)
				Destroy(this.gameObject);
		}
	}
}
