﻿using UnityEngine;
using System.Collections;

public class CameraTarget : LevelObject
{
//	protected int mPlayerIndex = -1;
//	public int PlayerIndex
//	{
//		set {mPlayerIndex = value;}
//		get {return mPlayerIndex;}
//	}
//	
//	protected Player mPlayer = null;

	public CameraBounds CameraBounds = null;
	
	protected override void Start()
	{
		base.Start();
		
//		mPlayer = mLevelManager.PlayerGet(mPlayerIndex);
		
		//Detach
		transform.parent = null;
	}
	
	protected override void LateUpdate()
	{
		base.LateUpdate();
		
//		if (mPlayer)
//			transform.position = mPlayer.transform.position;

		//Cap position to bounds limit
		if (CameraBounds != null)
		{
			Vector3 pos = transform.position;
			
			if (pos.x < CameraBounds.Min.position.x)
				pos.x = CameraBounds.Min.position.x;
			if (pos.y < CameraBounds.Min.position.y)
				pos.y = CameraBounds.Min.position.y;
			if (pos.z < CameraBounds.Min.position.z)
				pos.z = CameraBounds.Min.position.z;

			if (pos.x > CameraBounds.Max.position.x)
				pos.x = CameraBounds.Max.position.x;
			if (pos.y > CameraBounds.Max.position.y)
				pos.y = CameraBounds.Max.position.y;
			if (pos.z > CameraBounds.Max.position.z)
				pos.z = CameraBounds.Max.position.z;
				
			transform.position = pos;
		}
	}
}
