﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraBounds : LevelObject
{
	protected Transform mMinObj = null;
	public Transform Min 					{get{return mMinObj;}}
	
	protected Transform mMaxObj = null;
	public Transform Max 					{get{return mMaxObj;}}
	
	protected override void Awake()
	{
		base.Awake();

		mMinObj = transform.FindChild("Min");
		mMaxObj = transform.FindChild("Max");
	}
	
	protected override void OnDrawGizmos()
	{
		base.OnDrawGizmos();
		
		Gizmos.color = Color.green;
		
		Vector3 size = mMaxObj.position - mMinObj.position;
		Vector3 center = mMinObj.position + size*0.5f;
		
		Gizmos.DrawWireCube(center, size);
	}
}
