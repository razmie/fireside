﻿using UnityEngine;
using System.Collections;

public class LevelObject : Generic
{
	protected LevelManager mLevelManager = null;
	public LevelManager GetLevelManager()				{return mLevelManager;}
	
	protected override void Awake()
	{
		mLevelManager = FindObjectOfType<LevelManager>();
	}
}
