using UnityEngine;
using System.Collections;

public class LevelManager : Generic
{
//	public enum State
//	{
//		None = 0,
//		Setup,
//		Play,
//		Success,
//		GameOver,
//		Finish,
//	};
//
//	public const int PLAYER_MAX = 2;
	
	public static int LAYER_DEFAULT = 0;
	public static int LAYER_ENEMY = 0;
	public static int LAYER_PLAYER = 0;
//
//	public GameObject PlayerPrefab = null;
//	public GameObject TargetPrefab = null;
//	
//	//
//	protected State mState = State.None;
//	
//	protected Player[] mPlayers = new Player[PLAYER_MAX];
//	protected Target[] mTargets = new Target[PLAYER_MAX];
	
	protected CameraPlayer mCameraPlayer = null;

//	protected NPCManager mNPCManager = null;
//	public NPCManager NPCManager						{get {return mNPCManager;}}
//
//	protected WaveManager mWaveManager = null;
//	public WaveManager WaveManager							{get {return mWaveManager;}}
//
//	protected FXManager mFXManager = null;
//	public FXManager FXManager								{get{return mFXManager;}}
//
//	protected TimeManager mTimeManager = null;
//	public TimeManager TimeManager							{get{return mTimeManager;}}		
//						
//	protected GUILevel mGUILevel = null;
//	public GUILevel GUILevel								{get {return mGUILevel;}}
//	
//	protected LevelData mLevelData = new LevelData();
//	public LevelData LevelData								{get{return mLevelData;}}
//
//	protected Room mCurrentRoom = null;
//	public Room CurrentRoom
//	{
//		set {mCurrentRoom = value;}
//		get {return mCurrentRoom;}
//	}
//	
//	protected RoomConfig mCurrentRoomConfig = null;
//	public RoomConfig CurrentRoomConfig						{get{return mCurrentRoomConfig;}}
//
//	protected float mSpawnTime = 0.0f;
//	protected static float SPAWN_TIME_MAX = 2.0f;
//	
//	protected bool mCheckObjective = false;
//
//	//
	
	protected override void Awake()
	{
		base.Awake();
//		
//		if (PlayerPrefab == null)		Debug.LogError("PlayerPrefab is null!");
//		
//		//
//
//		for (int i = 0; i < PLAYER_MAX; i++)
//			mPlayers[i] = null;
//		
//		
//		mNPCManager = GetComponentInChildren<NPCManager>();
//		mWaveManager = GetComponentInChildren<WaveManager>();
//		mFXManager = GetComponentInChildren<FXManager>();
//		mTimeManager = GetComponentInChildren<TimeManager>();
//		mGUILevel = GetComponentInChildren<GUILevel>();
		
		mCameraPlayer = GetComponentInChildren<CameraPlayer>();
		
		LAYER_DEFAULT = 1 << LayerMask.NameToLayer("Default");
		LAYER_ENEMY = 1 << LayerMask.NameToLayer("Enemy");
		LAYER_PLAYER = 1 << LayerMask.NameToLayer("Player");
//
//		//Find if there's a room first
//		mCurrentRoom = FindObjectOfType<Room>();
//		
//		//Load room config
//		PlayConfigCollection collection = GameManager.Instance.PlayConfigCollection;
//		if (collection)
//		{
//			PlayConfig playConfig;
//			
//			if (collection.GetPlayConfig(PlayConfigType.Test, out playConfig))
//			{
//				LevelConfig levelConfig = playConfig.LevelConfigArray[0];
//				if (levelConfig != null)
//				{			
//					//Spawn room config object
//					mCurrentRoomConfig = (RoomConfig)GameObject.Instantiate(levelConfig.RoomConfigPrefab);
//					
//					//Spawn room if doesn't exist
//					if (mCurrentRoom == null)
//						mCurrentRoom = (Room)GameObject.Instantiate(levelConfig.RoomPrefab);
//				}
//			}
//		}
//		
//		if (mCurrentRoomConfig == null)
//			Debug.LogWarning("Can't find RoomConfig!");
//		
//		if (mCurrentRoom == null)
//			Debug.LogWarning("Can't find Room!");
	}
	
//	protected override void Start()
//	{
//		base.Start();
//		
//		mLevelData.Init();
//		
//		StateSetSetup();
//	}
//	
//	protected override void Update()
//	{
//		base.Update();
//		
//		if (InputController.Down(InputType.Start))
//		{
//			if (IsPaused())
//				PauseSet(false);
//			else
//				PauseSet(true);
//		}
//		
//		//
//		if (mState == State.Setup)
//		{
//			mSpawnTime -= Time.deltaTime;
//			if (mSpawnTime <= 0.0f)
//			{	
//				mSpawnTime = 0.0f;
//				
//				StateSetPlay();
//			}
//		}
//		
//		if (mCheckObjective)
//		{
//			mCheckObjective = false;
//			
//			CheckObjective();
//		}
//		
//		//
//		if (DBG.Data.DebugText)
//		{
//			GUIPanelDebug panel = mGUILevel.PanelDebug;
//			if (panel)
//			{
//				panel.Add("---LevelManager--");
//				panel.Add("State=" + mState);
//				panel.Add("SpawnTime=" + string.Format("{0:0.00}", mSpawnTime));
//				panel.Add("Screen=" + Screen.width + "x" + Screen.height);
//			}
//		}
//	}
//	
//	//
//	
//	public void OnEnemyDidKill(Enemy enemy)
//	{
//		mCheckObjective = true;
//	}
//
//	
//	
//	//
//	
//	public void PauseSet(bool pause)
//	{
//		if (mTimeManager)
//			mTimeManager.PauseSet(pause);
//	}
//	
//	public bool IsPaused()
//	{
//		if (mTimeManager)
//			return mTimeManager.IsPaused;
//		return false;
//	}
//
//	public void StateSetSetupRetry()
//	{
//		//Clear enemies
//		mNPCManager.EnemyDestroyAll();
//		
//		StateSetSetup();
//	}
//		
//	public void StateSetSetup()
//	{
//		//Reset
//		GUILevel.PanelObjShow(true);
//		GUILevel.PanelGameOverShow(false);
//		
//		//Spawn players if don't exist
//		if (PlayerGet(0) == null)
//			PlayerSpawn(mCurrentRoom, 0);
//			
//		Player player = PlayerGet(0);
//		if (player)
//			player.SetState((int)PlayerStateType.Spawn);
//
//		mWaveManager.StateSetSetup();	
//		
//		mSpawnTime = SPAWN_TIME_MAX;
//
//
//		mState = State.Setup;
//	}
//	
//	public void StateSetPlay()
//	{
//		mState = State.Play;
//	}
//	
//	public void StateSetSuccess()
//	{
//		Debug.Log("Room cleared");
//		
//		mState = State.Success;
//	}
//
//	public void StateSetGameOver()
//	{
//		//Stop enemy spawning
//		mWaveManager.StateSetStop();
//		
//		mNPCManager.OnGameOver();
//		
//		//UI
//		mGUILevel.PanelObjShow(false);
//		mGUILevel.PanelGameOverShow(true);
//		
//		mState = State.GameOver;
//	}
//	
//	public void StateSetFinish()
//	{
//		Application.LoadLevel("Level");
//		
//		mState = State.Finish;
//	}
//	
//	public State StateGet()
//	{
//		return mState;
//	}
//
//	public void PlayerKill(int playerIndex)
//	{
//		//Remove player
//		if (playerIndex < 0 || playerIndex >= PLAYER_MAX)
//		{
//			Debug.LogError("Player index is invalid");
//			return;
//		}
//		
//		if (mPlayers[playerIndex])
//		{
//			GameObject.Destroy(mPlayers[playerIndex].gameObject);
//			mPlayers[playerIndex] = null;
//		}
//	
//		//Game over
//		StateSetGameOver();
//		
//		Debug.Log("Killed player");
//	}
//	
//	public void PlayerSpawn(Room room, int playerIndex)
//	{
//		if (playerIndex < 0 || playerIndex >= PLAYER_MAX)
//		{
//			Debug.LogError("Player index is invalid");
//			return;
//		}
//		
//		if (room == null)
//			return;
//		if (room.PlayerSpawn == null)
//			return;
//			
//		if (DBG.Data.DontSpawnPlayer)
//		{
//			//Only spawn target
//			
//			if (mTargets[playerIndex] == null)
//			{
//				//Find first
//				Target target = FindObjectOfType<Target>();
//				
//				if (target == null)
//				{
//					GameObject obj = (GameObject)Instantiate(TargetPrefab, Vector3.zero, Quaternion.identity);
//					target = obj.GetComponent<Target>();
//				}
//				
//				mTargets[playerIndex] = target;
//				mTargets[playerIndex].PlayerIndex = playerIndex;
//			}
//		}
//		else
//		{
//			//Spawn player
//			
//			if (mPlayers[playerIndex] == null)
//			{
//				//Find first
//				Player player = FindObjectOfType<Player>();
//	
//				if (player == null)
//				{
//					GameObject obj = HierarchicalPrefabUtility.Instantiate(PlayerPrefab, Vector3.zero, Quaternion.identity);
//					player = obj.GetComponent<Player>();
//				}
//				
//				mPlayers[playerIndex] = player;
//				mPlayers[playerIndex].PlayerIndex = playerIndex;
//				
//				//Get target
//				mTargets[playerIndex] = player.GetComponentInChildren<Target>();
//				mTargets[playerIndex].PlayerIndex = playerIndex;
//			}
//		}
//			
//		if (mCameraPlayer)
//			mCameraPlayer.LookAtObj = mTargets[playerIndex].gameObject;
//	}
//
//	public Player PlayerGet(int playerIndex)
//	{
//		if (playerIndex < 0 || playerIndex >= PLAYER_MAX)
//		{
//			Debug.LogError("Player index is invalid");
//			return null;
//		}
//		
//		return mPlayers[playerIndex];
//	}
//
//	public Target TargetGet(int playerIndex)
//	{
//		if (playerIndex < 0 || playerIndex >= PLAYER_MAX)
//		{
//			Debug.LogError("Player index is invalid");
//			return null;
//		}
//		
//		return mTargets[playerIndex];
//	}
//	
//	public void CameraShake(float intensity, float swingSpeed, float decay, float roll)
//	{
//		if (mCameraPlayer)
//			mCameraPlayer.Shake(intensity, swingSpeed, decay, roll);
//	}
//	
//	public bool CameraIsShaking()
//	{
//		if (mCameraPlayer)
//			return mCameraPlayer.IsShaking();
//		return false;
//	}
//	
//	protected void CheckObjective()
//	{
//		if (mState == State.Success)
//			return;
//			
//		if (mCurrentRoomConfig.ObjectiveType == ObjectiveType.Kill)
//		{
//			if (mLevelData.TotalKillCount >= mCurrentRoomConfig.TotalKillCount)
//			{
//				StateSetSuccess();
//			}
//		}
//	}
}
