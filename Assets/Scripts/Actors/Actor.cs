﻿using UnityEngine;
using System.Collections;

public enum ActorStateType
{
	None = 0,
	Max,
}

public class Actor : LevelObject
{
	protected ActorMesh mActorMesh = null;
	public ActorMesh ActorMesh								{get{return mActorMesh;}}
	
	protected ActorData mActorData = null;

	public Vector3 Gravity = Vector3.zero;

	protected Animator mAnimator = null;
	public Animator Animator								{get{return mAnimator;}}
	public AnimatorStateInfo CurAnimState;
	public AnimatorStateInfo NextAnimState;
	protected int mPrevCurAnimHash = 0;
	protected int mPrevNextAnimHash = 0;
	
	protected CapsuleCollider mCapsuleColl = null;
	public CapsuleCollider CapsuleColl						{get{return mCapsuleColl;}}
	protected float mOriCollHeight = 0.0f;
	public float OriCollHeight								{get{return mOriCollHeight;}}

	protected const int MAX_STATES = 32;
	protected ActorState[] mActorStates = new ActorState[MAX_STATES];
	protected ActorState mCurActorState = null;
	public ActorState CurActorState 						{get{return mCurActorState;}}
	protected int mNextState = -1;
	
	protected Vector3 mRayOriginGround = Vector3.zero;
	protected RaycastHit mRayHitGround = new RaycastHit();
	public RaycastHit RayHitGround 							{get{return mRayHitGround;}}

	public Shader HitShader = null;
	protected int mHitCount = 0;
	protected float mHitWhiteTimer = 0.0f;
	protected Renderer[] mRenderers = null;
	protected Material[] mOriginalMaterials = null;
	protected bool mResetHitToOriMat = false;
	
	protected override void Awake()
	{
		base.Awake();
		
		mActorMesh = GetComponentInChildren<ActorMesh>();
		mAnimator = mActorMesh.GetComponent<Animator>();
		
		mCapsuleColl = GetComponent<CapsuleCollider>();
		if (mCapsuleColl)
			mOriCollHeight = mCapsuleColl.height;

		mRenderers = GetComponentsInChildren<Renderer>();
		
		mOriginalMaterials = new Material[mRenderers.Length];
		for (int i = 0; i < mRenderers.Length; i++)
		{
			if (mRenderers[i])
				mOriginalMaterials[i] = mRenderers[i].sharedMaterial;
		}
		
		mActorData = GetActorData();
	}
	
	protected override void Start()
	{
		base.Start();
		
		//Set initial direction
		mActorData.Direction = transform.forward;
		transform.rotation = Quaternion.LookRotation(mActorData.Direction);
	}
	
	protected override void Update()
	{
		base.Update();
		
		//Do ground hit ray check
		mRayOriginGround = transform.position + Vector3.up;
		float distance = 1.1f;
		int layerMask = LevelManager.LAYER_DEFAULT;
		if (Physics.SphereCast(mRayOriginGround, 0.1f, Vector3.down, out mRayHitGround, distance, layerMask) == false)
			mRayHitGround.point = mRayOriginGround + Vector3.down * distance;
		
		//Hit timer

		if (mHitWhiteTimer > 0.0f)
		{
			mHitWhiteTimer -= Time.deltaTime;
			if (mHitWhiteTimer <= 0.0f)
			{
				mHitWhiteTimer = 0.0f;
				mResetHitToOriMat = true;
			}
		}
		
		if (mResetHitToOriMat)
		{
			mResetHitToOriMat = false;

			//Change material back
			
			for (int i = 0; i < mRenderers.Length; i++)
			{
				if (mRenderers[i])
					mRenderers[i].material = mOriginalMaterials[i];
			}
			
		}
		
		//Update state
		
//		if (mNextState != -1)
//		{
//			SetStateActual(mNextState);
//			
//			mNextState = -1;
//		}
		
		if (mCurActorState != null)
			mCurActorState.Update();
	}
	
	protected override void LateUpdate()
	{
		base.LateUpdate();
		
		//Get anim states
		CurAnimState = Animator.GetCurrentAnimatorStateInfo(0);
		NextAnimState = Animator.GetNextAnimatorStateInfo(0);
		
		if (mPrevNextAnimHash != NextAnimState.fullPathHash &&
		    NextAnimState.fullPathHash != 0)
		{
			if (mCurActorState != null)
				mCurActorState.OnAnimWillChange(CurAnimState.fullPathHash, NextAnimState.fullPathHash);
			
			mPrevNextAnimHash = NextAnimState.fullPathHash;
		}
		
		if (mPrevCurAnimHash != CurAnimState.fullPathHash)
		{
			if (mCurActorState != null)
				mCurActorState.OnAnimDidChange(mPrevCurAnimHash, CurAnimState.fullPathHash);
			
			mPrevCurAnimHash = CurAnimState.fullPathHash;
		}
		
		//
		if (mCurActorState != null)
			mCurActorState.LateUpdate();
	}
		
	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		
		//
		if (mCurActorState != null)
			mCurActorState.FixedUpdate();
	}
	
	public void CallOnAnimatorIK()						{OnAnimatorIK();}
	protected virtual void OnAnimatorIK()
	{
		//
		if (mCurActorState != null)
			mCurActorState.OnAnimatorIK();
	}
	
	protected override void OnDrawGizmos()
	{
		//
		if (mCurActorState != null)
			mCurActorState.OnDrawGizmos();
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		
		//Reset
		mHitCount = 0;
		mHitWhiteTimer = 0.0f;
		
		//
		if (mActorMesh)
			mActorMesh.gameObject.SetActive(true);
	}
	
	protected override void OnDisable()
	{
		base.OnDisable();
		
		//
		if (mActorMesh)
			mActorMesh.gameObject.SetActive(false);
	}

	public virtual void OnCollisionEnter(Collision collision)
	{
		//
		if (mCurActorState != null)
			mCurActorState.OnCollisionEnter(collision);
	}
	
	public virtual void OnCollisionStay(Collision collision)
	{
		//
		if (mCurActorState != null)
			mCurActorState.OnCollisionStay(collision);
	}
	
	public virtual void OnAnimEvent(string name)
	{
		//
		if (mCurActorState != null)
			mCurActorState.OnAnimEvent(name);
	}
	
	public virtual void OnDidDamage()
	{
	}
	
	public virtual void OnStateEndTime(int state)
	{
	}
	
	//
	protected virtual ActorData GetActorData()
	{
		return null;
	}
	
	public ActorState SetState(int state)
	{
		mNextState = state;
		
		ActorState actorState = mActorStates[(int)mNextState];
		actorState.OnWillSet();
		
		//Set immediately if first time
//		if (mCurActorState == null)
//		{
//			SetStateActual(mNextState);
//			mNextState = -1;
//		}

		if (mNextState != -1)
		{
			SetStateActual(mNextState);
			
			mNextState = -1;
		}

		return actorState;
	}
	
	protected void SetStateActual(int state)
	{
		//Debug.Log("Set state to " + state);
		
		if (mCurActorState != null)
			mCurActorState.OnWillEnd();
		
		mCurActorState = mActorStates[(int)state];
		if (mCurActorState == null)
			Debug.LogError("mCurActorState is NULL!");
		
		if (mCurActorState != null)
			mCurActorState.OnDidStart();
	}
	
	protected int GetStateType()
	{
		if (mCurActorState != null)
			return mCurActorState.GetStateType();
		
		return -1;
	}
	
	public ActorState GetState(int state)
	{
		return mActorStates[state];
	}
	
	public void SetColliderHeight(float height)
	{
		CapsuleCollider coll = mCapsuleColl;
		
		//Change height of collider
		coll.height = height;
		
		Vector3 cen = coll.center;
		cen.y = coll.height * 0.5f;
		if (cen.y < coll.radius)
			cen.y = coll.radius;
		coll.center = cen;
	}
	
	public bool IsGrounded()
	{
		if (mRayHitGround.collider)
			return true;
		return false;
	}

	public void SetMaterialToColor(float time, Color color)
	{
		mHitWhiteTimer = time;
		
		if (HitShader && mRenderers != null)
		{
			//Colour object white
			
			for (int i = 0; i < mRenderers.Length; i++)
			{
				if (mRenderers[i])
				{					
					//Set shader
					mRenderers[i].material.shader = HitShader;
					mRenderers[i].material.color = color;
				}
			}
		}
	}

	public virtual float GetHealth()
	{
		return mActorData.Health;
	}
		
	public virtual void SetHealth(float health)
	{
		mActorData.Health = health;
	}
	
	public virtual bool CanDamage()
	{
		return true;
	}
	
	public virtual void AddDamage(float damage)
	{
		if (CanDamage() == false)
			return;
			
		if (mActorData.Health > 0.0f)
		{
			mActorData.Health -= damage;
			if (mActorData.Health < 0.0f)
				mActorData.Health = 0.0f;
				
			OnDidDamage();
		}
	}
	
	public bool IsAlive()
	{
		if (mActorData.Health == 0.0f)
			return false;
		return true;
	}
}
