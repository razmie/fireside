﻿using UnityEngine;
using System.Collections;

public class ActorState
{
	protected Actor mActor = null;
	
	protected float mEndTime = 0.0f;
	public float EndTime									{set{mEndTime=value;}}
	
	public ActorState(Actor actor)
	{
		mActor = actor;
	}

	public virtual void OnWillSet()
	{
	}
		
	public virtual void OnDidStart()
	{
	}
	
	public virtual void OnWillEnd()
	{
	}
	
	public virtual void OnAnimWillChange(int animFrom, int animTo)
	{
	}
	
	public virtual void OnAnimDidChange(int animFrom, int animTo)
	{
	}
	
	public virtual void Update()
	{
		if (mEndTime > 0.0f)
		{
			mEndTime -= Time.deltaTime;
			if (mEndTime <= 0.0f)
			{
				mEndTime = 0.0f;
				
				//
				mActor.OnStateEndTime(GetStateType());
			}
		}
	}
	
	public virtual void FixedUpdate()
	{
	}
	
	public virtual void LateUpdate()
	{
	}
	
	public virtual void OnAnimatorIK()
	{
	}
	
	public virtual void OnDrawGizmos()
	{
	}
	
	public virtual void OnCollisionEnter(Collision collision)
	{
	}

	public virtual void OnCollisionStay(Collision collision)
	{
	}
	
	public virtual void OnCollisionExit(Collision collision)
	{
	}
	
	public virtual void OnAnimEvent(string name)
	{
	}

	//
	public virtual int GetStateType()
	{
		return 0;
	}
}
