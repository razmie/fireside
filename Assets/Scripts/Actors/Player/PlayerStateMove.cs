using UnityEngine;
using System.Collections;

public class PlayerStateMove : PlayerStateMotion
{
	public PlayerStateMove(Actor actor) : base(actor)
	{
	}
	
//	public override void Update()
//	{
//		base.Update();
//		
//		if (mData.InpMag == 0.0f &&
//		    mData.CurrentSpeed < 0.1f)
//		{
//			mPlayer.SetState((int)PlayerStateType.Stand);
//			
//			mRigidbody.velocity = Vector3.zero;
//		}
//	}
	
	//
	public override int GetStateType()
	{
		return (int)PlayerStateType.Move;
	}
	
	protected override float GetSpeed()
	{
		return mData.SPEED_RUN;
	}
}