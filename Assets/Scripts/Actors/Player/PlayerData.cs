﻿using UnityEngine;
using System.Collections;

public class PlayerData : ActorData
{
	public float SPEED_RUN = 5.5f;
	
	public float SLOPE_ANGLE = 90.0f;

//	public Vector2 InpDirection = Vector2.zero;
//	public float InpMag = 0.0f;
	
	//
	public Vector2 AnimMoveDir = new Vector2();
//	public PlayerAnim.KnockType AnimKnockType = PlayerAnim.KnockType.None;
//	public PlayerAnim.SceneType AnimSceneType = PlayerAnim.SceneType.None;

	//
	public Vector3 Desired = Vector3.zero;
	public float CurrentSpeed = 0.0f;
	
	public int PlatInstanceID = 0;
	public Vector3 PlatPrevVel = Vector3.zero;
	public Vector3 PlatVelChange = Vector3.zero;
	
	//
//	public Enemy LastEnemyCollided = null;
}
