﻿using UnityEngine;
using System.Collections;

public static class PlayerAnim
{
	public enum Stand
	{
		None = 0,
		Idle1,
	}
	
	public enum Move
	{
		None = 0,
		Run,
	}
	
	public enum KnockType
	{
		None = 0,
		Backward,
		Forward,
		Down,
	}

	public enum SceneType
	{
		None = 0,
		Teleport,
	}
	
	public static int ANIM_RUN_GUN = 0;	
	public static int ANIM_SHOOT = 0;
	
	public static void SetupAnims()
	{
		ANIM_RUN_GUN		 	= Animator.StringToHash("Move.RunGun");
		ANIM_SHOOT				= Animator.StringToHash("Body.Rig|BodyShoot");
	}
}
