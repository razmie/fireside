﻿using UnityEngine;
using System.Collections;

public class PlayerState : ActorState
{
	protected Player mPlayer = null;
	protected PlayerData mData = null;
	protected Transform mTransform = null;
	protected Rigidbody mRigidbody = null;
	protected PlayerMesh mPlayerMesh = null;
	
	protected LevelManager mLevelManager = null;
	
	public PlayerState(Actor actor) : base(actor)
	{
		mPlayer = (Player)actor;
		mData = mPlayer.Data;
		mTransform = mPlayer.transform;
		mRigidbody = mPlayer.GetComponent<Rigidbody>();
		mPlayerMesh = (PlayerMesh)mPlayer.ActorMesh;
		
		mLevelManager = mPlayer.GetLevelManager();
	}
	
	public override void Update()
	{
		base.Update();

		mPlayer.Animator.SetFloat("MoveDirX", mData.AnimMoveDir.x);
		mPlayer.Animator.SetFloat("MoveDirY", mData.AnimMoveDir.y);
	}

	public virtual void OnDidShoot()
	{
		mPlayer.Animator.Play(PlayerAnim.ANIM_SHOOT, 1);
	}
}
