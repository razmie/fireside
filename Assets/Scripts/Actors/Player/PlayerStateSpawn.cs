﻿using UnityEngine;
using System.Collections;

public class PlayerStateSpawn : PlayerStateMotion
{
	public float mSpawnTimer = 0.0f;
	
	public PlayerStateSpawn(Actor actor) : base(actor)
	{
	}
	
//	public override void OnDidStart()
//	{
//		base.OnDidStart();
//		
//		//Reset
//		mData.AnimKnockType = PlayerAnim.KnockType.None;
//		mPlayer.Animator.Play(PlayerAnim.ANIM_RUN_GUN, 0, 0.0f);
//		
//		mPlayer.Animator.SetLayerWeight(1, 1.0f);
//		Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), false);
//		
//		mPlayer.SetHealth(1.0f);
//		
//		//
//		mSpawnTimer = 1.0f;
//		
//		//Set position
//		Vector3 position = mLevelManager.CurrentRoom.PlayerSpawn.position + Vector3.up * 2.0f;
//		mTransform.position = position;
//		mTransform.rotation = mLevelManager.CurrentRoom.PlayerSpawn.rotation;
//	}
	
	public override void Update()
	{
		base.Update();
		
		if (mSpawnTimer > 0.0f)
		{
			mSpawnTimer -= Time.deltaTime;
			if (mSpawnTimer <= 0.0f)
			{
				mSpawnTimer = 0.0f;
				
				mPlayer.SetState((int)PlayerStateType.Stand);
			}
		}
	}
	
	//
	public override int GetStateType()
	{
		return (int)PlayerStateType.Spawn;
	}
	
	protected override float GetSpeed()
	{
		return 0.0f;
	}
	
	protected override float GetSpeedMax()
	{
		return 0.0f;
	}
}