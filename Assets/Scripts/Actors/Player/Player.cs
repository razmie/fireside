﻿using UnityEngine;
using System.Collections;

public enum PlayerStateType
{
	Spawn = ActorStateType.Max,
	Stand,
	Move,
	Death,
	
	Max,
}

[RequireComponent (typeof(CapsuleCollider))]
public class Player : Actor
{
	protected PlayerData mData = new PlayerData();
	public PlayerData Data								{get{return mData;}}
	protected override ActorData GetActorData()			{return mData;}
	
	protected override void Awake()
	{
		base.Awake();

		mCapsuleColl = GetComponent<CapsuleCollider>();
		
		PlayerAnim.SetupAnims();
				
		//States
		mActorStates[(int)PlayerStateType.Spawn] = new PlayerStateSpawn(this);
		mActorStates[(int)PlayerStateType.Stand] = new PlayerStateStand(this);
		mActorStates[(int)PlayerStateType.Move] = new PlayerStateMove(this);
		mActorStates[(int)PlayerStateType.Death] = new PlayerStateDeath(this);
	}

	protected override void Update()
	{
		base.Update();
		
		//Debug
//		if (DBG.Data.DebugTextPlayer)
//		{
//			GUIPanelDebug panel = mLevelManager.GUILevel.PanelDebug;
//			if (panel)
//			{
//				panel.Add("---Player---");
//				
//				if (mCurActorState != null)
//					panel.Add("State=" + mCurActorState.GetType());
//				
//				panel.Add("Health=" + string.Format("{0:0.00}", mData.Health));
//			}
//		}
	}

//	public void OnEnemyDidCollide(Collision collision, Enemy enemy)
//	{
//		//Flash			
//		if (collision.contacts.Length > 0)
//		{
//			ContactPoint contact = collision.contacts[0];
//			Vector3 pos = contact.point + Vector3.up*0.25f;
//			mLevelManager.FXManager.Spawn(FX.Type.FlashHitPlayer, pos, contact.normal);
//		}
//		
//		//
//		mData.LastEnemyCollided = enemy;
//		
//		AddDamage(1.0f);
//	}
	
	public void OnDidShoot()
	{
		if (mCurActorState != null &&
		    mCurActorState is PlayerState)
			((PlayerState)mCurActorState).OnDidShoot();
	}
	
	public override bool CanDamage()
	{
		if (DBG.Data.InvinciblePlayer)
			return false;

		return base.CanDamage();
	}

//	public override void OnDidDamage()
//	{
//		base.OnDidDamage();
//		
//		if (GetHealth() == 0.0f)
//		{
//			PlayerStateKnock stateKnock = (PlayerStateKnock)SetState((int)PlayerStateType.Knock);
//			if (stateKnock != null)
//				stateKnock.EndTime = 1.25f;
//		}
//	}

//	public override void OnStateEndTime(int state)
//	{
//		base.OnStateEndTime(state);
//		
//		if (state == (int)PlayerStateType.Knock)
//		{
//			if (GetHealth() > 0.0f)
//				SetState((int)PlayerStateType.Stand);
//			else
//			{
//				SetState((int)PlayerStateType.Death);
//				
//				mLevelManager.StateSetGameOver();
//			}
//		}
//		else if (state == (int)PlayerStateType.Teleport)
//		{
//			mLevelManager.StateSetFinish();
//		}
//	}
}
