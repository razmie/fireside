﻿using UnityEngine;
using System.Collections;

public class PlayerStateStand : PlayerStateMotion
{
	public PlayerStateStand(Actor actor) : base(actor)
	{
	}
	
//	public override void Update()
//	{
//		base.Update();
//		
//		if (mData.InpMag > 0.0f)
//			mPlayer.SetState((int)PlayerStateType.Move);
//	}
	
	//
	public override int GetStateType()
	{
		return (int)PlayerStateType.Stand;
	}
}