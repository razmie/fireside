﻿using UnityEngine;
using System.Collections;

public class PlayerStateMotion : PlayerState
{
	protected static MultiRay mRayWall1 = new MultiRay(1);
	protected static MultiRay mRayWall2 = new MultiRay(1);
	
	protected static bool mHasHitWall = false;
	protected static RaycastHit mWallhit;
	protected static Vector3 mWallDir;
	protected static Vector3 mWallHitOrigin;
	
	protected static Vector3 mPrevPosition = Vector3.zero;
	protected static Quaternion mPrevRotation = Quaternion.identity;
	protected static Vector3 mPrevUp = Vector3.up;
	
	protected static Vector3[] mHitDirs = new Vector3[16];
	
	protected float mSlopeMag = 1.0f;
	
	public PlayerStateMotion(Actor actor) : base(actor)
	{
	}
	
//	public override void Update()
//	{
//		base.Update();
//
//		//Get input direction
//		mData.InpDirection = InputController.Axis(AxisType.Move);
//		mData.InpMag = mData.InpDirection.magnitude;
//
//		//Set direction
//		if (mData.InpMag > 0.0f)
//		{
//			//Get direction based on camera
//			
//			Vector3 inpDir;
//			inpDir.x = mData.InpDirection.x;
//			inpDir.y = 0.0f;
//			inpDir.z = mData.InpDirection.y;
//			inpDir.Normalize();
//			
//			Quaternion quatFrom, quatTo;
//			quatFrom = Quaternion.LookRotation(mData.Direction);
//			quatTo = Quaternion.LookRotation(inpDir);
//			quatFrom = quatTo;//Quaternion.RotateTowards(quatFrom, quatTo, Time.deltaTime * mData.SPEED_ROTATION);
//			
//			mData.Direction = quatFrom * Vector3.forward;
//		}
//
//		//Anim direction
//		Quaternion quat;
//		quat = Quaternion.FromToRotation(mPlayer.AimerTopDown.Direction, mData.Direction);
//		
//		Vector3 animDir = quat * Vector3.forward * mData.InpMag;
//		Vector2 targAnimDir = new Vector2(animDir.x, animDir.z);
//		
//		mData.AnimMoveDir = Vector2.Lerp(mData.AnimMoveDir, targAnimDir, Time.deltaTime*15.0f);
//		
//		//Check if on a slope. Slow down if so
//		mSlopeMag = 1.0f;
//		
//		if (mPlayer.IsGrounded())
//		{
//			float slopeAngle = Vector3.Angle(Vector3.up, mPlayer.RayHitGround.normal);
//			slopeAngle = Mathf.Min(slopeAngle, 90.0f);
//			
//			mSlopeMag = 1.0f - (slopeAngle/90.0f);
//		}
//		
//		//If ground is a rigidbody
//		if (mPlayer.RayHitGround.collider &&
//		    mPlayer.RayHitGround.collider.GetComponent<Rigidbody>())
//		{
//			if (mData.PlatInstanceID != mPlayer.RayHitGround.collider.gameObject.GetInstanceID())
//				mData.PlatInstanceID = mPlayer.RayHitGround.collider.gameObject.GetInstanceID();
//		}
//		else
//		{
//			//Reset
//			mData.PlatInstanceID = 0;
//			mData.PlatVelChange = Vector3.zero;
//			mData.PlatPrevVel = Vector3.zero;
//		}
//	}
//
//	public override void FixedUpdate()
//	{
//		base.FixedUpdate();
//
//		float speed = GetSpeed();
//		float speedMax = GetSpeedMax();
//		Vector3 velocity = mRigidbody.velocity;
//		
//		Vector3 target = mData.Direction * mData.InpMag * speed * mSlopeMag;
//		
//		//Smooth acceleration
//		mData.Desired = Vector3.Lerp(mData.Desired, target, Time.deltaTime*40.0f);
//		
//		mHasHitWall = FindWallWithDesired(ref target, 0.01f, 0.1f,
//		                                  ref mRayWall1);
//		   	
//		//If has hit wall find another desired
//		if (mHasHitWall)
//		{
//			float angNormMag = 0.0f;
//			
//			Vector3 angNormal = mWallhit.normal * Vector3.Cross(mData.Direction, mWallhit.normal).magnitude;
//			angNormMag = angNormal.magnitude;
//			
//			target += mWallDir * mData.InpMag * speed * angNormMag;
//			
//			//Check if hit another wall along wall direction
//			if (FindWallWithDesired(ref target, 0.01f, 0.1f,
//			                        ref mRayWall2))
//			{
//				//Stop dead
//				target = Vector3.zero;
//			}
//
//			//Smooth acceleration
//			mData.Desired = Vector3.Lerp(mData.Desired, target, Time.deltaTime*40.0f);
//		}
//
//		//If on platform
//		if (mPlayer.RayHitGround.collider &&
//		    mPlayer.RayHitGround.collider.GetComponent<Rigidbody>())
//		{
//			Vector3 platVel = mPlayer.RayHitGround.collider.GetComponent<Rigidbody>().GetPointVelocity(mPlayer.RayHitGround.point);
//			mData.PlatVelChange = platVel - mData.PlatPrevVel;
//			mData.PlatPrevVel = platVel;
//			
//			//Take out from velocity since we need to calculate desired difference
//			velocity -= platVel;
//		}
//		
//		//
//		Vector3 velChange = mData.Desired - velocity;
//		
//		velChange.x = Mathf.Clamp(velChange.x, -speedMax, speedMax);
//		velChange.z = Mathf.Clamp(velChange.z, -speedMax, speedMax);
//		velChange.y = 0.0f;
//
//		mRigidbody.AddForce(velChange, ForceMode.VelocityChange);
//
//		//Add gravity
//		mRigidbody.AddForce(mPlayer.Gravity * mRigidbody.mass);
//		
//		//Apply rotation
//		if (mPlayer.AimerTopDown.Direction.sqrMagnitude > 0.0f)
//			mRigidbody.rotation = Quaternion.LookRotation(mPlayer.AimerTopDown.Direction, Vector3.up);
//		
//		//
//		mData.CurrentSpeed = mRigidbody.velocity.magnitude;
//		
//		mPrevPosition = mTransform.position;
//		mPrevRotation = mTransform.rotation;
//		mPrevUp = mTransform.up;
//	}

	public override void LateUpdate()
	{
		base.LateUpdate();
		
		//Set mesh
		mPlayerMesh.transform.position = mTransform.position;
		mPlayerMesh.transform.rotation = mTransform.rotation;
	}
	
	public override void OnDrawGizmos()
	{
		base.OnDrawGizmos();
		
		Vector3 position = mTransform.position + mTransform.up * mPlayer.CapsuleColl.height;
		
		Gizmos.color = Color.cyan;
		position.y += 0.01f;
		Gizmos.DrawLine(position, position + mData.Direction);
		
		Gizmos.color = Color.red;
		position.y += 0.01f;
		Gizmos.DrawLine(position, position + mRigidbody.velocity*0.1f);
		
		//
		
		mRayWall1.DrawGizmos();
		mRayWall2.DrawGizmos();

		if (mHasHitWall)
		{
			Gizmos.color = Utils.ColorBlue(0.5f);
			Gizmos.DrawWireSphere(mWallhit.point, 0.02f);
			Gizmos.DrawLine(mWallhit.point, mWallhit.point+mWallhit.normal*0.1f);

			Gizmos.color = Utils.ColorYellow(0.5f);
			Vector3 pos = mPrevPosition + mPrevUp*mPlayer.CapsuleColl.height*0.5f;
			Gizmos.DrawLine(pos, pos+mWallDir);
			
			Utils.GizmoDrawCircle(pos, mPlayer.CapsuleColl.radius, 16);
			
			Gizmos.DrawWireSphere(mWallHitOrigin, 0.02f);
		}
	}
	
	//
	protected virtual float GetSpeed()
	{
		return 0.0f;
	}
	
	protected virtual float GetSpeedMax()
	{
		return mData.SPEED_RUN;
	}
	
	protected bool FindWallWithDesired(ref Vector3 desired,
	                                   float skin, float offsetBack,
	                                   ref MultiRay ray)
	{
		Vector3 direction = desired.normalized;
		 
//		float skin = 0.01f;
//		float offsetBack = 0.1f;
		float distance = (desired.magnitude * Time.fixedDeltaTime);
		distance += offsetBack;
		
		float radiusSkin = mPlayer.CapsuleColl.radius+skin;
		float height = mPlayer.CapsuleColl.height+skin*2.0f;
		
		Vector3 pos = mRigidbody.position +
						mTransform.up*mPlayer.CapsuleColl.center.y -
						direction*offsetBack;
		
		//Cast with capsule		
		ray.SetCapsuleStep(pos, radiusSkin, height,
		                         direction, distance, mTransform.up);
		
		if (ray.Cast(LevelManager.LAYER_DEFAULT))
		{
			//Find valid wall contact from list
			if (FindWallHit(ref ray, direction, ref mWallhit, ref mWallDir))
			{
				pos = mRigidbody.position;
				pos.y = mWallhit.point.y;
				
				//Find the beginning of wall hit to calculate real capsule cast distance
				FindLineCircleIntersections(pos, radiusSkin, mWallhit.point, -direction,
				                            ref mWallHitOrigin);
				
				mWallHitOrigin.y = pos.y;
				
				Vector3 capCastDist = mWallhit.point - mWallHitOrigin;
				
				desired = capCastDist / Time.fixedDeltaTime;
				
				return true;
			}
		}
		
		return false;
	}
	
	protected bool FindWallHit(ref MultiRay ray, Vector3 direction,
	                           ref RaycastHit hitOut, ref Vector3 wallDir)
	{
		float minDist = Mathf.Infinity;
		bool found = false;
		
		int hitDirCount = 0;
		
		for (int i = 0; i < ray.Step.Length; i++)
		{
			MultiRay.RayStep step = ray.Step[i];
			if (step.Hits != null)
			{
				for (int j = 0; j < step.Hits.Length; j++)
				{
					RaycastHit hit = step.Hits[j];
					
					if (hit.point.y > mTransform.position.y+mPlayer.CapsuleColl.radius-0.05f)
					{
						//If not slope
						if (Vector3.Angle(Vector3.up, hit.normal) > mData.SLOPE_ANGLE)
						{
							//If facing wall
							if (Vector3.Angle(-direction, hit.normal) < 90.0f)
							{
//								Vector3 hitToPly = hit.point - mTransform.position;
//								float hitFwdAng = Vector3.Angle(direction, hitToPly.normalized);
//								
//								if (hitFwdAng < 90.0f)
								{
									//Add to list
									if (hitDirCount < mHitDirs.Length)
									{
										bool canAdd = true;
										
										for (int k = 0; k < hitDirCount; k++)
										{
											if (Vector3.Angle(mHitDirs[k], hit.normal) < 1.0f)
											{
												canAdd = false;
												break;
											}
										}
										
										if (canAdd)
										{
											mHitDirs[hitDirCount] = hit.normal;
											hitDirCount++;
										}
									}
									
									if (hit.distance < minDist)
									{
										minDist = hit.distance;
										
										hitOut = hit;
										hitOut.normal = hit.normal;
										
										found = true;
									}
								}
							}
						}
					}
				}
			}
		}

		if (found)
		{
			Vector3 totalDir = Vector3.zero;
			for (int i = 0; i < hitDirCount; i++)
				totalDir += mHitDirs[i];

			hitOut.normal = totalDir.normalized;

			//
			float angle = Utils.AngleAroundAxis(direction, hitOut.normal, Vector3.up);

			wallDir = Vector3.Cross(hitOut.normal, Vector3.up).normalized;
			if (angle < 0.0f)
				wallDir = -wallDir;
		}
		
		return found;
	}
	
	protected int FindLineCircleIntersections(Vector3 circlePos, float radius,
	                                          Vector3 point1, Vector3 direction,
	                                          ref Vector3 intersection)
	{
		Vector2 cp = new Vector2(circlePos.x, circlePos.z);
		Vector2 p1 = new Vector2(point1.x, point1.z);
		Vector2 d = new Vector2(direction.x, direction.z);
		Vector2 int1;
		Vector2 int2;

		int ret = Utils.FindLineCircleIntersections(cp, radius, p1, d,
		            			                      out int1, out int2);
		            			                      
		intersection.x = int1.x;
		intersection.z = int1.y;

		return ret;
	}
}
