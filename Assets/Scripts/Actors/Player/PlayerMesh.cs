﻿using UnityEngine;
using System.Collections;

public class PlayerMesh : ActorMesh
{
	protected Player mPlayer = null;

	protected override void Awake()
	{
		base.Awake();
		
		mPlayer = transform.parent.GetComponent<Player>();
	}
	
	public void OnAnimEvent(string name)
	{
		if (mPlayer)
			mPlayer.OnAnimEvent(name);
	}
}
