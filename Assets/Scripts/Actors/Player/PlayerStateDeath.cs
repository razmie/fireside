﻿using UnityEngine;
using System.Collections;

public class PlayerStateDeath : PlayerState
{
	public PlayerStateDeath(Actor actor) : base(actor)
	{
	}
	
	public override void OnDidStart()
	{
		base.OnDidStart();

//		mData.AnimKnockType = PlayerAnim.KnockType.Down;
		
		mPlayer.Animator.SetLayerWeight(1, 0.0f);
		
		Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Enemy"), true);		
	}
	
	public override void LateUpdate()
	{
		base.LateUpdate();
		
		//Set mesh
		mPlayerMesh.transform.position = mTransform.position;
		mPlayerMesh.transform.rotation = mTransform.rotation;
	}
	
	//
	public override int GetStateType()
	{
		return (int)PlayerStateType.Death;
	}
}