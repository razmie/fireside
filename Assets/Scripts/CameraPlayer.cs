using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraPlayer : LevelObject 
{
	public GameObject LookAtObj = null;
	public Vector3 LookAtOffset = new Vector3(0, 0, 0);
	public Vector3 FromOffset = new Vector3(0, 10, -10);
	public float Distance = 100.0f;
	public float LerpSpeed = 10.0f;

	protected float mShakeDecay = 0.0f;
	protected float mShakePower = 0.0f;
	protected float mShakeSwingSpeed = 0.0f;
	protected float mShakeRoll = 0.0f;	
	protected Vector3 mShakeOffset = Vector3.zero;
	protected float mShakeTime = 0.0f;
	protected Vector3 mShakeDirection = Vector3.zero;
	
	protected Vector3 mPosition = Vector3.zero;
	protected Quaternion mRotation = Quaternion.identity;
	
	//
	private Vector3 mLookAtPos = Vector3.zero;
	
	protected override void Awake()
	{
		base.Awake();
		
		//Detach
		transform.parent = null;
	}
	
	protected override void Start()
	{
		base.Start();

		UpdatePositionAndLook(true);
	}
		
	protected override void LateUpdate()
	{
		base.LateUpdate();
		
		UpdatePositionAndLook();
	}
		
	//
	
	public void UpdatePositionAndLook(bool immediate = false)
	{
		if (LookAtObj)
		{
			if (immediate)
			{
				//Set position to target immediately
				mLookAtPos = LookAtObj.transform.position + LookAtOffset;
			}
			else
			{
				//Lerp to new position
				mLookAtPos += ((LookAtObj.transform.position + LookAtOffset) - mLookAtPos) * Time.smoothDeltaTime * LerpSpeed;
			}
		}
		
		//Pos
		Vector3 newPos = mLookAtPos + (FromOffset.normalized * Distance);
		
		//Look at target
		Vector3 lookDir = mLookAtPos - newPos;
		lookDir.Normalize();
		
		if (lookDir == Vector3.zero)
		{
			Debug.LogWarning("Look dir is zero!");
			return;
		}
		
		Vector3 position;
		Quaternion rotation;
				
		if (mShakePower > 0.0f)
		{
			mShakeTime += Time.deltaTime * mShakeSwingSpeed;
			float s = Mathf.Sin(mShakeTime);
			
			mShakePower -= Time.deltaTime * mShakeDecay;
			if (mShakePower < 0.0f)
				mShakePower = 0.0f;

			position = newPos + mShakeDirection * s * mShakePower;
			rotation = Quaternion.LookRotation(lookDir) *
								Quaternion.Euler(mShakeDirection * s * mShakePower) *
								Quaternion.Euler(0, 0, mShakeDirection.magnitude * s * mShakePower * mShakeRoll);
		}
		else
		{
			position = newPos;
			rotation = Quaternion.LookRotation(lookDir);
		}
				
		transform.position = position;
		transform.rotation = rotation;
	}

	public void Shake(float power, float swingSpeed, float decay, float roll)
	{
		if (IsShaking() &&
		    power < mShakePower)
			return;
			
		mShakeDirection = (transform.up * Random.Range(-1.0f, 1.0f)) +
			(transform.right * Random.Range(-1.0f, 1.0f));
		mShakeDirection.Normalize();
		
		mShakePower = power;
		mShakeSwingSpeed = swingSpeed;
		mShakeDecay = decay;
		mShakeRoll = roll;
		
		mShakeTime = 0.0f;
	}
	public bool IsShaking()
	{
		if (mShakePower > 0.0f)
			return true;
		return false;
	}

//	protected void OnGUI()
//	{
//		if (GUI.Button(new Rect(20,40,80,20), "Shake"))
//		{
//			Shake(0.25f, 20.0f, 0.75f, 10.0f);
//		}
//	}
}