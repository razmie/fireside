﻿Shader "Custom/RimSpecular"
{
    Properties
    {
     	_MainColor ("Main Color", Color) = (1.0,1.0,1.0,0.0)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
     	_RimColor ("Rim Color", Color) = (1.0,1.0,1.0,0.0)
      	_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
    }
    
    SubShader
    {
    	Tags { "RenderType" = "Opaque" }
    	
      	CGPROGRAM
      	#pragma surface surf BlinnPhong
      	
      	struct Input
      	{
      		//float4 color : COLOR;
          	float3 viewDir;
      	};
      	
      	float4 _MainColor;
      	half _Shininess;
      	float4 _RimColor;
      	float _RimPower;
      	
      	void surf (Input IN, inout SurfaceOutput o)
      	{
         	o.Albedo = _MainColor.rgb;
         	o.Gloss = _SpecColor.a;
         	o.Specular = _Shininess;
          	half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
          	o.Emission = _RimColor.rgb * pow (rim, _RimPower);
      	}
      	ENDCG
    } 
    Fallback "Specular"
}
