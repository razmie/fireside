﻿Shader "Custom/Rim"
{
    Properties
    {
     	_MainColor ("Main Color", Color) = (1.0,1.0,1.0,0.0)
     	_RimColor ("Rim Color", Color) = (1.0,1.0,1.0,0.0)
      	_RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
    }
    
    SubShader
    {
    	Tags { "RenderType" = "Opaque" }
    	
      	CGPROGRAM
      	#pragma surface surf Lambert
      	
      	struct Input
      	{
      		float4 color : COLOR;
          	float3 viewDir;
      	};
      	
      	float4 _MainColor;
      	float4 _RimColor;
      	float _RimPower;
      	
      	void surf (Input IN, inout SurfaceOutput o)
      	{
         	o.Albedo = _MainColor.rgb;
          	half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
          	o.Emission = _RimColor.rgb * pow (rim, _RimPower);
      	}
      	ENDCG
    } 
    Fallback "Diffuse"
}
