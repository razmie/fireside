﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[InitializeOnLoad]
public class ObjectMover : Editor
{
	protected static bool mSelected = false;
	protected static Transform mSelectedTransform = null;

	static ObjectMover()
	{
		mSelected = false;
		
		SceneView.onSceneGUIDelegate += SceneGUI;
	}
	
	static void SceneGUI(SceneView sceneView)
	{
		Event cur = Event.current;		
		
		Ray ray = HandleUtility.GUIPointToWorldRay(cur.mousePosition);

		Vector3 selectedPoint = Vector3.zero;
		
		if (mSelectedTransform)
		{
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				selectedPoint = hit.point;
				
				Handles.color = Color.red;
				Handles.DrawLine(hit.point, hit.point+Vector3.up);
				
				HandleUtility.Repaint();
			}
		}

		if (IsCancelKeyDown(cur))
		{
			mSelectedTransform = null;
		}

		if (Selection.activeTransform &&
		    mSelectedTransform == null)
		{
			if (IsKeyDown(cur))
			{
				mSelectedTransform = Selection.activeTransform;
			}
		}
		else if (mSelectedTransform)
		{
//			if (IsKeyDown(cur) ||
//				cur.type == EventType.MouseDown &&
//			    cur.button == 0)

			if (IsKeyDown(cur))
			{
				Undo.RecordObject(mSelectedTransform, "Set selected position");
				mSelectedTransform.position = selectedPoint;
				
				mSelectedTransform = null;
			}
		}
	}

	static bool IsCancelKeyDown(Event cur)
	{
		if (cur.type == EventType.KeyUp &&
		    cur.keyCode == KeyCode.Escape)
		    return true;
		return false;
	}
	
	static bool IsKeyDown(Event cur)
	{
		if (cur.type == EventType.KeyUp &&
		    cur.keyCode == KeyCode.X &&
		    cur.control)
		    return true;
		return false;
	}
}
