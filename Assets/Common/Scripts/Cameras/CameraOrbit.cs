﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour
{
	public GameObject LookAtObj = null;
	public float Distance = 50.0f;
	
	public float RotationSensitivity = 200;
	
	public bool ClampHorzAngle = false;
	public float ClampHorzMin = 0.0f;
	public float ClampHorzMax = 0.0f;
	
	//
	protected float mRotHorzAngle = 0.0f;
	protected float mRotVertAngle = 0.0f;
	
	protected Vector3 mStartForward = Vector3.forward;
	
	protected void Start()
	{
		//mStartForward = transform.forward;
	}
	
	protected void FixedUpdate()
	{
		//Get angle rotation from mouse
		mRotVertAngle += Input.GetAxis("Mouse X") * RotationSensitivity * Time.fixedDeltaTime;
		mRotHorzAngle += Input.GetAxis("Mouse Y") * RotationSensitivity * Time.fixedDeltaTime;
		
		if (ClampHorzAngle)
			mRotHorzAngle = Mathf.Clamp(mRotHorzAngle, ClampHorzMin, ClampHorzMax);	
			
		//Clamp so that it can only look straight up or down
		mRotHorzAngle = Mathf.Clamp(mRotHorzAngle, -89.99f, 89.99f);

		Quaternion quat;
		//quat = Quaternion.LookRotation(mStartForward);
		quat = Quaternion.AngleAxis(mRotVertAngle, Vector3.up);
		quat *= Quaternion.AngleAxis(mRotHorzAngle, Vector3.right);
		
		transform.position = quat * Vector3.forward * Distance;
		
		if (LookAtObj)
			transform.LookAt(LookAtObj.transform.position);
	}
}
