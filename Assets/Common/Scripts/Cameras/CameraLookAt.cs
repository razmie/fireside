using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class CameraLookAt : MonoBehaviour 
{
	public GameObject LookAtObj = null;
	public Vector3 PositionOffset = new Vector3(0, 10, -10);
	public Vector3 LookAtOffset = new Vector3(0, 0, 0);
	public float Distance = 100.0f;
	public float LerpSpeed = 10.0f;
	
	//
	private Vector3 mLookAtPos = Vector3.zero;
	public Vector3 LookAtPos		{set {newLookAtPos = value;} get {return newLookAtPos;}}
	
	private Vector3 newLookAtPos = Vector3.zero;

	void Start()
	{
		if (LookAtObj)
		{
			mLookAtPos = newLookAtPos = LookAtObj.transform.position;
		}
		
		LookAtTarget();
		
	}
	
	void Update()
	{
		if (LookAtObj)			
			newLookAtPos = LookAtObj.transform.position;
		
		if (LerpSpeed == 0.0f)
		{
			//Immediate
			mLookAtPos = newLookAtPos;
		}
		else
		{
			//Smooth out
			mLookAtPos = mLookAtPos + (newLookAtPos - mLookAtPos) * Time.deltaTime * LerpSpeed;
		}

		LookAtTarget();
	}

	//
	
	protected void LookAtTarget()
	{
		Vector3 cameraPos;
			
		cameraPos = mLookAtPos + (PositionOffset.normalized * Distance);		
		transform.position = cameraPos;
		
		//Look at target
		transform.LookAt(mLookAtPos + LookAtOffset);
	}
}