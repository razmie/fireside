﻿using UnityEngine;
using System.Collections;

public class RadiusDragger : MonoBehaviour
{
	public GameObject TargetObj = null;
	public float MaxDragDistance = 1.0f;
	public float MoveSmoother = 5;
	
	//
	protected float mMaxDragDistanceSqr = 0.0f;

	protected void Awake()
	{
		mMaxDragDistanceSqr = MaxDragDistance * MaxDragDistance;
	}
	
	protected void Update()
	{
		if (TargetObj)
		{
			Vector3 direction = TargetObj.transform.position - transform.position;
			float distSqr = direction.sqrMagnitude;
			
			if (distSqr > mMaxDragDistanceSqr)
			{
				direction.Normalize();
			
				Vector3 position = TargetObj.transform.position - (direction * MaxDragDistance);
				
				transform.position = transform.position + ((position - transform.position) * Time.deltaTime * MoveSmoother);
			}
		}
	}
	
	protected void OnDrawGizmos()
	{
		if (Application.isPlaying == false)
			return;
		
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, 0.125f);
		
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(TargetObj.transform.position, 0.125f);
		
		Gizmos.DrawLine(transform.position, TargetObj.transform.position);
	}
}
