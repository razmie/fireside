using UnityEngine;
using System.Collections;

public class CharBuffer : MonoBehaviour
{
	[HideInInspector]
    public string buffer = "";
 
    private const int mSize = 32;
 
    void Update()
    {
        for (KeyCode keyCode = KeyCode.A; keyCode <= KeyCode.Z; keyCode++)
        {
            if (Input.GetKeyDown(keyCode))
            {
				if (buffer.Length >= mSize)
                    buffer = buffer.Substring(1, buffer.Length-1);
               
                buffer += ((char)(((int)keyCode - (int)KeyCode.A) + (int)('A')));
            }
        }
 
        for (KeyCode keyCode = KeyCode.Alpha0; keyCode <= KeyCode.Alpha9; keyCode++)
        {
            if (Input.GetKeyDown(keyCode))
            {
				if (buffer.Length >= mSize)
                    buffer = buffer.Substring(1, buffer.Length - 1);
               
                buffer += "" + ((int)keyCode - (int)KeyCode.Alpha0);
            }
        }
    }
 
    public bool Contains(string match)
    {
        if (buffer.Contains(match.ToUpper()))
        {
            buffer = "";
            return true;
        }
        return false;
    }
}
