﻿using UnityEngine;
using System.Collections;

public class MultiRay
{
	public enum CastType
	{
		Ray = 0,
		Sphere,
		Capsule
	}

	public class RayStep
	{
		public CastType CastType = CastType.Ray;
	
		public Vector3 Position = Vector3.zero;
		public Vector3 Direction = Vector3.zero;
		public float Distance = 0.0f;
		
		public float Radius = 0.0f;
		
		public float Height = 0.0f;
		public Vector3 Up = Vector3.zero;

		//
		public bool IsHit = false;
		public Vector3 HitPos = Vector3.zero;
		public Vector3 HitNormal = Vector3.zero;
		public Collider HitCollider = null;

		public RaycastHit[] Hits = null;
		
		public void Reset()
		{
			Position = Vector3.zero;
			Direction = Vector3.zero;
			Distance = 0.0f;

			Radius = 0.0f;
			
			Height = 0.0f;
			Up = Vector3.zero;
		}
	}
	
	public RayStep[] mStep = null;
	public RayStep[] Step					{get{return mStep;}}
	
	protected int mStepIndex = 0;
	
	protected bool mIsHit = false;
	public int HitCount = 0;
	protected RaycastHit mRaycastHit = new RaycastHit();
	public RaycastHit RaycastHit			{get{return mRaycastHit;}}
	
	public MultiRay(int length)
	{
		mStep = new RayStep[length];
		for (int i = 0; i < length; i++)
			mStep[i] = new RayStep();
	}
	
	public void DrawGizmos()
	{
		Color col;
	
		if (mIsHit)
			col = Color.red;
		else
			col = Color.green;
			
		for (int i = 0; i < mStep.Length; i++)
		{
			MultiRay.RayStep step = Step[i];
			
			col.a = 0.5f;
			Gizmos.color = col;
			
			if (step.CastType == CastType.Ray)
				Gizmos.DrawLine(step.Position, step.Position + step.Direction * step.Distance);
			else if (step.CastType == CastType.Sphere)
			{
				Gizmos.DrawWireSphere(step.Position, step.Radius);
				
				if (step.Distance > 0.0f)
				{
					Gizmos.DrawWireSphere(step.Position + step.Direction * step.Distance, step.Radius);
					Gizmos.DrawLine(step.Position, step.Position + step.Direction * step.Distance);
				}
			}
			else if (step.CastType == CastType.Capsule)
			{
				Utils.GizmoDrawCapsule(step.Position, step.Radius, step.Height, step.Direction, step.Up, step.Distance);
			}

			//if (step.IsHit)
			{
				col.a = 0.5f;
				Gizmos.color = col;
				
				if (step.Hits != null)
				{
					for (int j = 0; j < step.Hits.Length; j++)
					{
						Gizmos.DrawWireSphere(step.Hits[j].point, 0.01f);
						Gizmos.DrawLine(step.Hits[j].point, step.Hits[j].point + step.Hits[j].normal*0.05f);
					}
				}
				
//				Gizmos.DrawWireSphere(step.HitPos, 0.01f);
//				Gizmos.DrawLine(step.HitPos, step.HitPos + step.HitNormal*0.1f);
			}
		}
	}
	
	//
	public Vector3 SetStep(Vector3 position, Vector3 direction, float distance)
	{
		Vector3 finalPos;
		
		if (mStepIndex < mStep.Length)
		{
			Step[mStepIndex].Reset();
			
			mStep[mStepIndex].CastType = CastType.Ray;
		
			mStep[mStepIndex].Position = position;
			mStep[mStepIndex].Direction = direction;
			mStep[mStepIndex].Distance = distance;
			
			finalPos = position + direction * distance;
			
			mStepIndex++;
		}
		else
		{
			Debug.LogError("Not enough steps!");
			
			finalPos = position;
		}
			
		return finalPos;
	}

	public Vector3 AddStep(Vector3 direction, float distance)
	{
		if (mStepIndex > 0 &&
		    mStepIndex < mStep.Length)
		{
			int prevIndex = mStepIndex-1;
			
			return SetStep(mStep[prevIndex].Position + mStep[prevIndex].Direction * mStep[prevIndex].Distance,
			       			direction,
			        		distance);
		}

		Debug.LogError("Cannot add step!");
		return Vector3.zero;
	}
	
	public void SetSphereStep(Vector3 position, float radius, Vector3 direction, float distance)
	{
		if (mStepIndex < mStep.Length)
		{
			Step[mStepIndex].Reset();
			
			mStep[mStepIndex].CastType = CastType.Sphere;
			
			mStep[mStepIndex].Position = position;
			mStep[mStepIndex].Radius = radius;
			mStep[mStepIndex].Direction = direction;
			mStep[mStepIndex].Distance = distance;
			
			mStepIndex++;
		}
		else
		{
			Debug.LogError("Not enough steps!");
		}
	}
	
	public void SetCapsuleStep(Vector3 position, float radius, float height, Vector3 direction, float distance, Vector3 up)
	{
		if (mStepIndex < mStep.Length)
		{
			Step[mStepIndex].Reset();
			
			mStep[mStepIndex].CastType = CastType.Capsule;
			
			mStep[mStepIndex].Position = position;
			mStep[mStepIndex].Height = height;
			mStep[mStepIndex].Radius = radius;
			mStep[mStepIndex].Direction = direction;
			mStep[mStepIndex].Distance = distance;
			mStep[mStepIndex].Up = up;
			
			mStepIndex++;
		}
		else
		{
			Debug.LogError("Not enough steps!");
		}
	}
	
	public void AddSphereStep(float radius, Vector3 direction, float distance)
	{
		if (mStepIndex > 0 &&
		    mStepIndex < mStep.Length)
		{
			int prevIndex = mStepIndex-1;
			
			SetSphereStep(mStep[prevIndex].Position + mStep[prevIndex].Direction * mStep[prevIndex].Distance,
			              radius, direction, distance);
		}
		else
			Debug.LogError("Cannot add step!");
	}

	public void AddCapsuleStep(float radius, float height, Vector3 direction, float distance, Vector3 up)
	{
		if (mStepIndex > 0 &&
		    mStepIndex < mStep.Length)
		{
			int prevIndex = mStepIndex-1;
			
			SetCapsuleStep(mStep[prevIndex].Position + mStep[prevIndex].Direction * mStep[prevIndex].Distance,
			               radius, height, direction, distance, up);
		}
		else
			Debug.LogError("Cannot add step!");
	}
	
	public bool Cast(int layerMask)
	{
		mIsHit = false;
		HitCount = 0;
		
		for (int i = 0; i < mStep.Length; i++)
			mStep[i].IsHit = false;

		for (int i = 0; i < mStepIndex; i++)
		{
			if (mStep[i].CastType == CastType.Ray)
			{
				mStep[i].Hits = Physics.RaycastAll(mStep[i].Position,
									mStep[i].Direction,
									mStep[i].Distance,
									layerMask);
									
				if (FindNearest(ref mStep[i].Hits, ref mRaycastHit))
				{
					mStep[i].IsHit = true;
					mStep[i].HitPos = mRaycastHit.point;
					mStep[i].HitNormal = mRaycastHit.normal;
					mStep[i].HitCollider = mRaycastHit.collider;
					
					mIsHit = true;
					HitCount+=mStep[i].Hits.Length;
					break;
				}
			}
			else if (mStep[i].CastType == CastType.Sphere)
			{
				if (mStep[i].Distance > 0.0f)
				{
					mStep[i].Hits = Physics.SphereCastAll(mStep[i].Position,
				                       mStep[i].Radius,
				                       mStep[i].Direction,
				                       mStep[i].Distance,
				                       layerMask);

					//CalcRealNormals(mStep[i].Hits);
					
					if (FindNearest(ref mStep[i].Hits, ref mRaycastHit))
					{
						mStep[i].IsHit = true;
						mStep[i].HitPos = mRaycastHit.point;
						mStep[i].HitNormal = mRaycastHit.normal;
						mStep[i].HitCollider = mRaycastHit.collider;
						
						mIsHit = true;
						HitCount+=mStep[i].Hits.Length;
						break;
					}
				}
			}
			else if (mStep[i].CastType == CastType.Capsule)
			{
				if (mStep[i].Distance > 0.0f)
				{
					float rad2 = mStep[i].Radius*2.0f;
					float heightRad;
					if (mStep[i].Height < rad2) 	heightRad = 0;
					else 				heightRad = mStep[i].Height-rad2;

					float hh = heightRad*0.5f;
					Vector3 p1 = mStep[i].Position-mStep[i].Up*hh;
					Vector3 p2 = mStep[i].Position+mStep[i].Up*hh;
				
					mStep[i].Hits = Physics.CapsuleCastAll(p1, p2,
				                   	mStep[i].Radius,
				                   	mStep[i].Direction,
				                   	mStep[i].Distance,
				                   	layerMask);

					//CalcRealNormals(mStep[i].Hits);
					
					if (FindNearest(ref mStep[i].Hits, ref mRaycastHit))
					{
						mStep[i].IsHit = true;
						mStep[i].HitPos = mRaycastHit.point;
						mStep[i].HitNormal = mRaycastHit.normal;
						mStep[i].HitCollider = mRaycastHit.collider;
						
						mIsHit = true;
						HitCount+=mStep[i].Hits.Length;
						break;
					}
				}
			}
		}

		//
		mStepIndex = 0;
		
		return mIsHit;
	}
	
	protected bool FindNearest(ref RaycastHit[] hits, ref RaycastHit nearest)
	{
		if (hits == null)
			return false;

		float minDist = Mathf.Infinity;
		int index = -1;
		
		for (int i = 0; i < hits.Length; i++)
		{
			if (hits[i].distance < minDist)
			{
				minDist = hits[i].distance;
				index = i;
			}
		}
		
		if (index > -1)
		{
			nearest = hits[index];
			return true;
		}
		
		return false;
	}
	
	protected void CalcRealNormals(RaycastHit[] hits)
	{
		RaycastHit tempHit;
		for (int i = 0; i < hits.Length; i++)
		{
			if (Physics.Raycast(hits[i].point+hits[i].normal*0.01f,
			                    -hits[i].normal, out tempHit))
				hits[i].normal = tempHit.normal;
		}
	}
}
