﻿using UnityEngine;
using System.Collections;

public class RadiusLerper : MonoBehaviour
{
	public GameObject TargetObj = null;
	public float MaxDragDistance = 1.0f;
	public float MoveSmoother = 5.0f;
	
	public BoxCollider BoxClamp = null;
	protected Vector3 mMin = Vector3.zero;
	protected Vector3 mMax = Vector3.zero;
	
	protected bool mLerp = false;
	
	//
	private static float mMaxDragDistanceSqr = 0.0f;
	
	protected void Awake()
	{
		mMaxDragDistanceSqr = MaxDragDistance * MaxDragDistance;
		
		if (BoxClamp)
		{
			mMin = BoxClamp.transform.position + BoxClamp.size * 0.5f;
			mMax = BoxClamp.transform.position - BoxClamp.size * 0.5f;
		}
	}
	
	protected void LateUpdate()
	{
		if (TargetObj)
		{
			Vector3 direction = TargetObj.transform.position - transform.position;
			float distSqr = direction.sqrMagnitude;
			
			if (distSqr > mMaxDragDistanceSqr)
				mLerp = true;
			
			if (mLerp)
			{
				Vector3 position = transform.position + ((TargetObj.transform.position - transform.position) * Time.deltaTime * MoveSmoother);
				
				//Clamp area
				if (BoxClamp)
				{
					position = Vector3.Min(position, mMin);
					position = Vector3.Max(position, mMax);
				}

				transform.position = position;
				
				if (distSqr < 0.1f)
					mLerp = false;
			}
		}
	}
	
	protected void OnDrawGizmos()
	{
		if (Application.isPlaying == false)
			return;
		
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, 0.125f);
		
		if (TargetObj)
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireSphere(TargetObj.transform.position, 0.125f);
			
			Gizmos.DrawLine(transform.position, TargetObj.transform.position);
		}
	}
}
