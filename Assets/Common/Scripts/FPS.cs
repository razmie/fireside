using UnityEngine;
using System.Collections;

[RequireComponent (typeof(GUIText))]

public class FPS : MonoBehaviour
{
	public float UpdateInterval = 0.5f;

	private float mAccum = 0.0f;	// FPS accumulated over the interval
	private int mFrames = 0;		// Frames drawn over the interval
	private float mTimeleft;		// Left time for current interval

	void Start()
	{
		if (!GetComponent<GUIText>())
		{
			Debug.Log("Missing guiText!");
			enabled = false;
			return;
		}

		GetComponent<GUIText>().fontSize = 20;
		GetComponent<GUIText>().color = Color.black;
		
		mTimeleft = UpdateInterval;	
	}
	
	void Update()
	{
		if (Time.timeScale == 0.0f)
			return;
			
		mTimeleft -= Time.deltaTime;
		mAccum += Time.timeScale/Time.deltaTime;
		++mFrames;
		
		// Interval ended - update GUI text and start new interval
		if (mTimeleft <= 0.0)
		{
			// display two fractional digits (f2 format)
			GetComponent<GUIText>().text = "" + (mAccum/mFrames).ToString("f2");
			mTimeleft = UpdateInterval;
			mAccum = 0.0f;
			mFrames = 0;
		}
	}
}
