﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class CollectionBase : MonoBehaviour
{
	protected virtual void Reset()
	{
		if (Application.isEditor == false)
			return;
		
		MapCreate();

		for (int i = 0; i < GetTypeCount(); i++)
		{
			string key = GetTypeString(i);
			MapAddData(key);
		}
	}
	
	public virtual void UpdateList()
	{
		//Add to map if type doesn't exist

		for (int i = 0; i < GetTypeCount(); i++)
		{
			string key = GetTypeString(i);
			if (MapContainsData(key) == false)
				MapAddData(key);
		}
		
		//Remove from map if not in type
		
		for (int i = 0; i < MapGetDataCount(); i++)
		{
			bool foundInType = false;
			string key = MapGetDataKey(i);
			
			for (int j = 0; j < GetTypeCount(); j++)
			{
				if (key == GetTypeString(j))
				{
					foundInType = true;
					break;
				}
			}
			
			if (foundInType == false)
				MapRemoveData(key);
		}
	}
	
	public virtual void MapCreate()
	{
	}
	
	public virtual void MapAddData(string key)
	{
	}

	public virtual void MapRemoveData(string key)
	{
	}
	
	public virtual bool MapContainsData(string key)
	{
		return false;
	}

	public virtual int MapGetDataCount()
	{
		return 0;
	}
	
	public virtual string MapGetDataKey(int index)
	{
		return "";
	}

	public virtual int GetTypeCount()
	{
		return 0;
	}
	
	public virtual string GetTypeString(int index)
	{
		return "";
	}
}
