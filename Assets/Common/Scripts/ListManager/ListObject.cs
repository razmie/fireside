﻿using UnityEngine;
using System.Collections;

public class ListObject : Generic
{
	private int mIndex = -1;
	public int Index
	{
		set {mIndex = value;}
		get {return mIndex;}
	}
	
	protected ListObjectManager mListObjectManager = null;
	public ListObjectManager ListObjectManager
	{
		set {mListObjectManager = value;}
		get {return mListObjectManager;}
	}
}
