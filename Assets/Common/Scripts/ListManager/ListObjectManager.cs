﻿using UnityEngine;
using System.Collections;

public class ListObjectManager : MonoBehaviour
{
	//
	protected ListObject[] mListObjects = null;
	protected int mListCount = 0;
	protected int mListCountMax = 0;

	protected virtual void Awake()
	{
		//Create array
		
		mListCountMax = GetCount();
		if (mListCountMax > 0)
			mListObjects = new ListObject[mListCountMax];
	}
	
	protected virtual void Update()
	{
	}
	
	protected virtual void OnDrawGizmos()
	{
	}
	
	//
	
	public virtual int GetCount()
	{
		return 0;
	}

	public bool CanRegisterListObject()
	{
		if (mListCount < mListCountMax)
			return true;
		return false;
	}
	
	public virtual int RegisterListObject(ListObject listObject)
	{
		if (mListCountMax == 0)
			Debug.LogWarning("mListCountMax == 0!");
			
		//Find availble position
		for (int i = 0; i < mListCountMax; i++)
		{
			if (mListObjects[i] == null)
			{
				mListObjects[i] = listObject;
				mListObjects[i].Index = i;
				mListObjects[i].ListObjectManager = this;
				
				mListCount++;
			
				//Return position index
				return i;
			}
		}
		
		Debug.LogWarning("Can't register list object!");
		return -1;
	}
	
	public virtual void DeregisterListObject(ListObject listObject)
	{
		int index = listObject.Index;
		
		if (IsIndexValid(index))
		{
			if (mListObjects[index] == listObject)
			{
				mListObjects[index].Index = -1;
				mListObjects[index].ListObjectManager = null;
				
				mListObjects[index] = null;
				
				mListCount--;
				if (mListCount < 0)
				{
					mListCount = 0;
					Debug.LogWarning("List object count is less than zero!");
				}
				
				return;
			}
			else
			{
				if (mListObjects[index] == null)
					Debug.Log("object is null");
				else
					Debug.Log("object is not the same");
			}
		}
		else
		{
			Debug.Log("index is invalid");
		}
		
		Debug.LogWarning("Can't deregister list object. Index is invalid or object is already null!" + mListCount + " " + index);
		
		//Go through list
		for (int i = 0; i < mListObjects.Length; i++)
		{
			if (mListObjects[i] == listObject)
			{
				mListObjects[i] = null;
				
				mListCount--;
				if (mListCount < 0)
				{
					mListCount = 0;
					Debug.LogWarning("List object count is less than zero!");
					return;
				}
			}
		}
		
		Debug.LogWarning("Failed to find object!");
	}

	
	public virtual void DeregisterAllListObjects()
	{
		for (int i = 0; i < mListObjects.Length; i++)
		{
			if (mListObjects[i])
			{
				mListObjects[i].Index = -1;
				mListObjects[i].ListObjectManager = null;
				mListObjects[i] = null;
			}
		}
	
		mListCount = 0;
	}
	
	public ListObject GetListObject(int index)
	{
		if (IsIndexValid(index) == false)
			return null;
		
		return mListObjects[index];
	}
	
	public int GetListObjectCount()
	{
		return mListCount;
	}
	
	protected bool IsIndexValid(int index)
	{
		if (index < 0 && index >= mListObjects.Length)
		{
			Debug.LogWarning("List object index is invalid!");
			return false;
		}
		
		return true;
	}
}
