using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class Utils
{
	public static T FindComponent<T>(string name) where T : Component
	{
		GameObject obj = GameObject.Find(name);
		if (obj)
			return obj.GetComponent<T>();
			
		
		return null;
	}

	public static T[] FindComponents<T>(string name) where T : Component
	{
		T[] comps = GameObject.FindObjectsOfType<T>();
		if (comps != null)
		{
			List<T> compsName = new List<T>();
		
			for (int i = 0; i < comps.Length; i++)
			{
				if (comps[i].gameObject.name == name)
					compsName.Add(comps[i]);
			}

			if (compsName.Count > 0)
				return compsName.ToArray(); 
		}
		
		return null;
	}
	
	public static T GetComponentInChildrenWithName<T>(this GameObject obj, string name) where T : Component
	{
		T[] comps = obj.GetComponentsInChildren<T>();
		if (comps != null)
		{
			for (int i = 0; i < comps.Length; i++)
			{
				if (comps[i].gameObject.name == name)
					return comps[i];
			}
		}
		
		return null;
	}

	public static GameObject FindChild(this GameObject obj, string name)
	{
		Transform transform = obj.transform.FindChild(name);
		if (transform)
			return transform.gameObject;
		return null;
	}
	
	public static GameObject FindInChildren(this GameObject obj, string name)
	{
		Transform transform = obj.GetComponentInChildrenWithName<Transform>(name);
		if (transform)
			return transform.gameObject;
		return null;
	}
	
	public static Transform FindInChildren(this Transform obj, string name)
	{
		return obj.gameObject.GetComponentInChildrenWithName<Transform>(name);
	}
	
	public static void DestroyChildrenWithComponent<T>(this GameObject obj) where T : Component
	{
		T[] comps = obj.GetComponentsInChildren<T>();
		if (comps != null)
		{
			if (Application.isPlaying == false)
			{
				for (int i = 0; i < comps.Length; i++)
				{
					if (comps[i])
					{
						if (obj.GetInstanceID() != comps[i].gameObject.GetInstanceID())
							GameObject.DestroyImmediate(comps[i].gameObject);
					}
				}
			}
			else
			{
				for (int i = 0; i < comps.Length; i++)
				{
					if (comps[i])
					{
						if (obj.GetInstanceID() != comps[i].gameObject.GetInstanceID())
							GameObject.Destroy(comps[i].gameObject);
					}
				}
			}
		}
	}
	
	//
	
	public static Vector3 PlaneRayIntersection(Plane plane, Ray ray)
	{
		float dist;
		plane.Raycast(ray, out dist);

		return ray.GetPoint(dist);
	}
	
//	public static Vector3 ScreenPointToPointOnPlane(Plane plane, Camera camera)
//	{
//		return ScreenPointToPointOnPlane(Input.mousePosition, plane, camera);
//	}

	public static Vector3 ScreenMousePointToPointOnPlane(Plane plane, Camera camera)
	{
		return ScreenPointToPointOnPlane(Input.mousePosition, plane, camera);
	}
	
	public static Vector3 ScreenPointToPointOnPlane(Vector3 screenPoint, Plane plane, Camera camera)
	{
		//Set up a ray corresponding to the screen position
		Ray ray = camera.ScreenPointToRay(screenPoint);
		
		//Find out where the ray intersects with the plane
		return PlaneRayIntersection(plane, ray);
	}
	
	public static bool ScreenPointToRaycastHit(Camera camera, int layerMask, out RaycastHit hit)
	{
		Ray ray = camera.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit, 1000.0f, layerMask))
			return true;
		
		return false;
	}
	
	public static T SpawnComponent<T>(UnityEngine.Object prefab) where T : Component
	{
		GameObject gameObject = (GameObject)GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
		if (gameObject)
		{
			T component = gameObject.GetComponent<T>();
			if (component)
				return component;
			else
				Debug.Log("Can't find component in prefab!");
		}
		else
			Debug.Log("Can't instantiate game object!");
		
		return null;
	}

	// The angle between dirA and dirB around axis
	public static float AngleAroundAxis(Vector3 dirA, Vector3 dirB, Vector3 axis)
	{
	    // Project A and B onto the plane orthogonal target axis
	    dirA = dirA - Vector3.Project(dirA, axis);
	    dirB = dirB - Vector3.Project(dirB, axis);
	   
	    // Find (positive) angle between A and B
	    float angle = Vector3.Angle(dirA, dirB);
	   
	    // Return angle multiplied with 1 or -1
	    return angle * (Vector3.Dot(axis, Vector3.Cross (dirA, dirB)) < 0 ? -1 : 1);
	}

//	public static float AngleBetweenDirections(Vector3 foward, Vector3 direction)
//	{
//	    // the vector perpendicular to referenceForward (90 degrees clockwise)
//	    // (used to determine if angle is positive or negative)
//	    Vector3 right = Vector3.Cross(Vector3.up, foward);
//	     
//	    // Get the angle in degrees between 0 and 180
//	    float angle = Vector3.Angle(direction, foward);
//	     
//	    // Determine if the degree value should be negative. Here, a positive value
//	    // from the dot product means that our vector is the right of the reference vector
//	    // whereas a negative value means we're on the left.
//	    float sign = (Vector3.Dot(direction, right) > 0.0f) ? 1.0f: -1.0f;
//	     
//	    return sign * angle;
//	}

	//
	
	public static float Cap(float value, float min, float max)
	{
		value = Mathf.Max(value, min);
		value = Mathf.Min(value, max);

		return value;
	}
	
	//
	
	public static float TweenQuadEaseInOut(float time, float timeMax, float start, float diff)
	{
		if (timeMax == 0.0f)
			return 1.0f;
			
		time /= timeMax/2;
		
		if (time < 1)
			return diff/2*time*time + start;
		time--;
		
		return -diff/2 * (time*(time-2) - 1) + start;
	}

	public static float TweenQuadEaseIn(float time, float timeMax, float start, float diff)
	{
		if (timeMax == 0.0f)
			return 1.0f;
			
		time /= timeMax;
		return diff*time*time + start;
	}
	
	public static float TweenQuadEaseOut(float time, float timeMax, float start, float diff)
	{
		if (timeMax == 0.0f)
			return 1.0f;
			
		time /= timeMax;
		return -diff * time*(time-2) + start;
	}

	//

	public static Vector3 Vector3TweenQuadEaseInOut(float time, float timeMax, Vector3 start, Vector3 diff)
	{
		if (timeMax == 0.0f)
			return start+diff;
		
		time /= timeMax/2;
		
		if (time < 1)
			return diff/2*time*time + start;
		time--;
		
		return -diff/2 * (time*(time-2) - 1) + start;
	}
	
	public static IEnumerator Wait(float duration)
	{
		for (float timer = 0; timer < duration; timer += Time.deltaTime)
			yield return 0;
			
		Debug.Log("DONE");
	}

	public static Vector3 ClosestPointOnLine(Vector3 start, Vector3 end, Vector3 point, bool capWithinLine = false)
	{
		Vector3 vVector1 = point - start;
		Vector3 vVector2 = (end - start).normalized;
		
		float d = Vector3.Distance(start, end);
		float t = Vector3.Dot(vVector2, vVector1);
		
		if (capWithinLine)
		{
			if (t <= 0)
				return start;
			
			if (t >= d)
				return end;
		}
		
		Vector3 vVector3 = vVector2 * t;
		
		Vector3 vClosestPoint = start + vVector3;
		
		return vClosestPoint;
	}
	
//	public static int FindLineCircleIntersections(Vector3 center, float radius,
//	                                              Vector3 point1, Vector3 point2,
//	                                              out Vector3 intersection1, out Vector3 intersection2)
//	{
//		float dx, dz, A, B, C, det, t;
//		
//		center.y = 0.0f;
//		point1.y = 0.0f;
//		point2.y = 0.0f;
//		
//		dx = point2.x - point1.x;
//		dz = point2.y - point1.y;
//		
//		A = dx * dx + dz * dz;
//		B = 2 * (dx * (point1.x - center.x) + dz * (point1.y - center.y));
//		C = (point1.x - center.x) * (point1.x - center.x) + (point1.y - center.y) * (point1.y - center.y) - radius * radius;
//		
//		det = B * B - 4 * A * C;
//		if ((A <= 0.0000001) || (det < 0))
//		{
//			// No real solutions.
//			intersection1 = new Vector3(float.NaN, float.NaN, float.NaN);
//			intersection2 = new Vector3(float.NaN, float.NaN, float.NaN);
//			return 0;
//		}
//		else if (det == 0)
//		{
//			// One solution.
//			t = -B / (2 * A);
//			intersection1 = new Vector3(point1.x + t * dx, 0.0f, point1.y + t * dz);
//			intersection2 = new Vector3(float.NaN, float.NaN, float.NaN);
//			return 1;
//		}
//		else
//		{
//			// Two solutions.
//			t = (float)((-B + Mathf.Sqrt(det)) / (2 * A));
//			intersection1 = new Vector3(point1.x + t * dx, 0.0f, point1.y + t * dz);
//			t = (float)((-B - Mathf.Sqrt(det)) / (2 * A));
//			intersection2 = new Vector3(point1.x + t * dx, 0.0f, point1.y + t * dz);
//			
//			//Get closest to point 1
//			if ((intersection2 - point1).sqrMagnitude < 
//			    (intersection1 - point1).sqrMagnitude)
//			{
//				Vector3 temp = intersection2;
//				intersection2 = intersection1;
//				intersection1 = temp;
//			}
//			
//			return 2;
//		}
//	}
                                            
	public static int FindLineCircleIntersections(Vector2 circlePos, float radius,
	                                              Vector2 point1, Vector2 direction,
	                                              out Vector2 intersection1, out Vector2 intersection2)
	{
		float A, B, C, det, t;
		
		A = direction.x * direction.x + direction.y * direction.y;
		B = 2 * (direction.x * (point1.x - circlePos.x) + direction.y * (point1.y - circlePos.y));
		C = (point1.x - circlePos.x) * (point1.x - circlePos.x) + (point1.y - circlePos.y) * (point1.y - circlePos.y) - radius * radius;
		
		det = B * B - 4 * A * C;
		if ((A <= 0.0000001) || (det < 0))
		{
			// No real solutions.
			intersection1 = new Vector2(float.NaN, float.NaN);
			intersection2 = new Vector2(float.NaN, float.NaN);
			return 0;
		}
		else if (det == 0)
		{
			// One solution.
			t = -B / (2 * A);
			intersection1 = new Vector2(point1.x + t * direction.x, point1.y + t * direction.y);
			intersection2 = new Vector2(float.NaN, float.NaN);
			return 1;
		}
		else
		{
			// Two solutions.
			t = (float)((-B + Math.Sqrt(det)) / (2 * A));
			intersection1 = new Vector2(point1.x + t * direction.x, point1.y + t * direction.y);
			
			t = (float)((-B - Math.Sqrt(det)) / (2 * A));
			intersection2 = new Vector2(point1.x + t * direction.x, point1.y + t * direction.y);
			
			//Get closest to point 1
			if ((intersection2 - point1).sqrMagnitude < 
			    (intersection1 - point1).sqrMagnitude)
			{
				Vector3 temp = intersection2;
				intersection2 = intersection1;
				intersection1 = temp;
			}
			
			return 2;
		}
	}

	public static Color ColorBlack(float alpha = 1)				{return new Color(0, 0, 0, alpha);}
	public static Color ColorWhite(float alpha = 1)				{return new Color(1, 1, 1, alpha);}	
	public static Color ColorGray(float alpha = 1)				{return new Color(0.5f, 0.5f, 0.5f, alpha);}
	public static Color ColorRed(float alpha = 1)				{return new Color(1, 0, 0, alpha);}
	public static Color ColorGreen(float alpha = 1)				{return new Color(0, 1, 0, alpha);}
	public static Color ColorBlue(float alpha = 1)				{return new Color(0, 0, 1, alpha);}
	public static Color ColorYellow(float alpha = 1)			{return new Color(1, 1, 0, alpha);}
	public static Color ColorCyan(float alpha = 1)				{return new Color(0, 1, 1, alpha);}
	public static Color ColorMagenta(float alpha = 1)			{return new Color(1, 0, 1, alpha);}

	public static Color ColorOrange(float alpha = 1)			{return new Color(1, 0.6f, 0, alpha);}
	public static Color ColorLimeGreen(float alpha = 1)			{return new Color(0.5f, 1, 0, alpha);}
	
	//
	public static void GizmoDrawCircle(Vector3 position, float radius, int resolution)
	{
		float minRad = (360.0f/(float)resolution) * Mathf.Deg2Rad;
		float radian = 0.0f;

		Vector3 v = Vector3.zero;
		Vector3 fv = Vector3.zero;
		
		for (int i = 0; i < resolution+1; i++)
		{
			v.x = position.x + (float)Math.Cos(radian)*radius;
			v.y = position.y;
			v.z = position.z + (float)Math.Sin(radian)*radius;
			
			if (i > 0)
				Gizmos.DrawLine(fv, v);
			
			radian += minRad;
			
			fv = v;
		}
	}
	
	public static void GizmoDrawArc(Vector3 position, float radius, int resolution,
									float startAngle, float angle,
	                                Vector3 forward, Vector3 right)
	{
		float minRad = (angle/(float)resolution) * Mathf.Deg2Rad;
		float radian = startAngle * Mathf.Deg2Rad;
		
		Vector3 v = Vector3.zero;
		Vector3 fv = Vector3.zero;

		for (int i = 0; i < resolution+1; i++)
		{
			v = position + (forward * (float)Math.Cos(radian)*radius) + (right * (float)Math.Sin(radian)*radius);
			
			if (i > 0)
				Gizmos.DrawLine(fv, v);
			
			radian += minRad;
			
			fv = v;
		}
	}
	
	public static void GizmoDrawCapsule(Vector3 pos, float radius, float height,
	                                    Vector3 forward, Vector3 up,
	                                    float distance = 0.0f)
	{
		Vector3 p1, p2;
		Vector3 f1, f2, b1, b2;
		Vector3 rf1, rf2, lf1, lf2;
		Vector3 right = Vector3.Cross(up, forward);

		float rad2 = radius*2.0f;
		float heightRad;
		if (height < rad2) 	heightRad = 0;
		else 				heightRad = height-rad2;
		
		Vector3 bottom = pos-up*heightRad*0.5f;
		Vector3 upHeight = up*heightRad;
		Vector3 forwDist = forward*distance;
		Vector3 rightRad = right*radius;
		Vector3 upRad = up*radius;
		int res = 16;
		int halfRes = 8;
		
		//Lines
		p1 = bottom + forward*(distance+radius);
		p2 = p1 + upHeight;
		Gizmos.DrawLine(p1, p2);

		p1 = bottom - forward*radius;
		p2 = p1 + upHeight;
		Gizmos.DrawLine(p1, p2);
		
		rf1 = bottom + rightRad;
		rf2 = rf1 + upHeight;
		Gizmos.DrawLine(rf1, rf2);

		lf1 = bottom - rightRad;
		lf2 = lf1 + upHeight;
		Gizmos.DrawLine(lf1, lf2);
		
		//Forward circles
		f1 = bottom + forwDist;
		GizmoDrawArc(f1, radius, res, -90, 180, forward, right);
		GizmoDrawArc(f1, radius, halfRes, 0, 90, forward, -up);
		
		f2 = f1 + upHeight;
		GizmoDrawArc(f2, radius, res, -90, 180, forward, right);
		GizmoDrawArc(f2, radius, halfRes, 0, 90, forward, up);
				
		//Back circles
		b1 = bottom;
		GizmoDrawArc(b1, radius, res, 90, 180, forward, right);
		GizmoDrawArc(b1, radius, res, -90, 180, -up, right);
		GizmoDrawArc(b1, radius, halfRes, 90, 90, forward, -up);
		
		b2 = b1 + upHeight;
		GizmoDrawArc(b2, radius, res, 90, 180, forward, right);
		GizmoDrawArc(b2, radius, res, -90, 180, up, right);
		GizmoDrawArc(b2, radius, halfRes, 90, 90, forward, up);
		
		//Mid
		if (distance > 0.0f)
		{
			p1 = rf1 + forwDist;
			Gizmos.DrawLine(rf1, p1);
			
			p2 = p1 + upHeight;
			Gizmos.DrawLine(p1, p2);
			Gizmos.DrawLine(rf2, p2);
			
			p1 = lf1 + forwDist;
			Gizmos.DrawLine(lf1, p1);
			
			p2 = p1 + upHeight;
			Gizmos.DrawLine(p1, p2);
			Gizmos.DrawLine(lf2, p2);

			p1 = b1 - upRad;
			p2 = p1 + forwDist;
			Gizmos.DrawLine(p1, p2);

			p1 = b2 + upRad;
			p2 = p1 + forwDist;
			Gizmos.DrawLine(p1, p2);
			
			GizmoDrawArc(f1, radius, res, -90, 180, -up, right);
			GizmoDrawArc(f2, radius, res, -90, 180, up, right);
		}
	}
	
	public static int GetUID()
	{
		int uniqueCounter = 0;
		string key = "UniqueCounter";
		
		uniqueCounter = PlayerPrefs.GetInt(key);
		
		uniqueCounter++;
		
		PlayerPrefs.SetInt(key, uniqueCounter);
		PlayerPrefs.Save();
		
		return uniqueCounter;
	}
}

public static class D
{	
	public static void L(params object[] objs)
	{
		string text = "";
		for (int i = 0; i < objs.Length; i++)
			text += objs[i].ToString() + " ";
		Debug.Log(text);
	}
	
	public static void Assert(UnityEngine.Object obj)
	{
		if (obj == null)
			Debug.LogError("NULL!");
	}
}

public static class MyExtensions
{
	public static bool IsZero(this Vector3 vec)
	{
		if (vec.sqrMagnitude <= 0.00001f)
			return true;
		return false;
	}
}   
