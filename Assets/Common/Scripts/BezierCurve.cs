﻿using UnityEngine;
using System.Collections;

public class BezierCurve
{
	[HideInInspector]
	public Vector3[] Pos = new Vector3[4];
	public const int SEGMENT_DRAW_COUNT = 16;
	
	public BezierCurve()
	{
		Pos[0] = Vector3.zero;
		Pos[1] = Vector3.zero;
		Pos[2] = Vector3.zero;
		Pos[3] = Vector3.zero; 
	}
	
	public void Set(Vector3 pos0, Vector3 pos1, Vector3 pos2, Vector3 pos3)
	{
		Pos[0] = pos0;
		Pos[1] = pos1;
		Pos[2] = pos2;
		Pos[3] = pos3;
	}
	
	public void Copy(BezierCurve bezierCurve)
	{
		Pos[0] = bezierCurve.Pos[0];
		Pos[1] = bezierCurve.Pos[1];
		Pos[2] = bezierCurve.Pos[2];
		Pos[3] = bezierCurve.Pos[3];
	}
	
	public Vector3 GetPoint(float t)
	{
		return CalculateBezierPoint(t, Pos[0], Pos[1], Pos[2], Pos[3]);
	}
	
	public Vector3 CalculateBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
	{
		float u = 1.0f - t;
		float tt = t*t;
		float uu = u*u;
		float uuu = uu * u;
		float ttt = tt * t;
		
		Vector3 p = uuu * p0; //first term
		p += 3 * uu * t * p1; //second term
		p += 3 * u * tt * p2; //third term
		p += ttt * p3; //fourth term
		
		return p;
	}
	
	public float CalculateLength(int segmentCount)
	{
		if (segmentCount == 0)
			return 0.0f;
			
		Vector3 pos0, pos1, diff;
		float lengthSqr = 0.0f;
		
		pos0 = Pos[0];
		
		for (int i = 1; i <= segmentCount; i++)
		{
			pos1 = GetPoint(i/segmentCount);
			
			diff = pos1 - pos0;
			lengthSqr += diff.sqrMagnitude;
			
			pos0 = pos1;
		}
		
		return Mathf.Sqrt(lengthSqr);
	}

	public void DrawGizmo()
	{
		Vector3 p0, p1;
		
		float t = 0.0f;
		
		p0 = GetPoint(0);

		 Gizmos.color = Color.green;
		 				 
		for(int i = 1; i <= SEGMENT_DRAW_COUNT; i++)
		{
		 	t = i / (float)SEGMENT_DRAW_COUNT;
		  	p1 = GetPoint(t);

			Gizmos.DrawLine(p0, p1);
		  
			p0 = p1;
		}
		
		//Gizmos.color = Color.yellow;
		
		Gizmos.DrawWireSphere(Pos[0], 0.25f);
		Gizmos.DrawWireSphere(Pos[1], 0.25f);
		Gizmos.DrawWireSphere(Pos[2], 0.25f);
		Gizmos.DrawWireSphere(Pos[3], 0.25f);
	}
}
