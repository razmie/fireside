﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]

public class MotorSteer : MonoBehaviour
{
	public float MoveSpeed = 10.0f;
	public float Gravity = 10.0f;
	
	//
	private CharacterController mController = null;
	
	private CollisionFlags mCollisionFlags;
	private float mVerticalSpeed = 0.0f;
		
	protected Vector3 mAcceleration = Vector3.zero;
	protected Vector3 mVelocity = Vector3.zero;
	
	protected Vector3 mMovement = Vector3.zero;
	public Vector3 Movement
	{
		get {return mMovement;}
	}

	protected virtual void Awake()
	{
		mController = gameObject.GetComponent<CharacterController>();
	}
	
	protected virtual void FixedUpdate()
	{
	}
	
	protected virtual void Update()
	{
		Reset();
		
		OnSteer();
		
		UpdateMovement();
	}
	
	protected virtual void OnSteer()
	{
	}
	
	protected virtual void OnDrawGizmos()
	{
		if (Application.isPlaying == false)
			return;
		
		Vector3 pos0, pos1;
				
		pos0 = transform.position;
		pos1 = transform.position + mAcceleration;

		Gizmos.color = Color.red;
		Gizmos.DrawLine(pos0, pos1);
	}
	    
	//
	
	public void UpdateMovement()
	{
		ApplyGravity();
		
		mVelocity = mVelocity + mAcceleration;
		mVelocity = Vector3.ClampMagnitude(mVelocity, MoveSpeed);
		
		mMovement = mVelocity + (new Vector3(0, mVerticalSpeed, 0));
		mMovement *= Time.deltaTime;;
		
		// Move the controller
		mCollisionFlags = mController.Move(mMovement);
	}

	protected void Reset()
	{
		mAcceleration = Vector3.zero;
	}

	public void Stop()
	{
		mAcceleration = Vector3.zero;
		mVelocity = Vector3.zero;
	}

	public void SteerArival(Vector3 targetPosition, float force, float breakDistance)
	{
		Vector3 desired = targetPosition - transform.position;
		
		float speed = MoveSpeed;

		float distance = desired.magnitude;
		if (distance > 0)
		{
			if (distance < breakDistance)
				speed = MoveSpeed * (distance/breakDistance);
			else
				speed = MoveSpeed;
			
			if (desired != Vector3.zero)
				desired.Normalize();
			desired = desired * speed;
			
			Vector3 steer = desired - mVelocity;
			
			mAcceleration += (steer / 1.0f);
			mAcceleration = Vector3.ClampMagnitude(mAcceleration, force);
		}
	}
	
	public void SteerDirection(Vector3 direction, float force)
	{
		Vector3 desired;
		float speed = MoveSpeed;
		
		desired = direction * speed;
	
		Vector3 steer = desired - mVelocity;
		
		mAcceleration += (steer / 1.0f);
		mAcceleration = Vector3.ClampMagnitude(mAcceleration, force);
	}
	
	protected void ApplyGravity()
	{
		if (IsGrounded ())
			mVerticalSpeed = 0.0f;
		else
			mVerticalSpeed -= Gravity * 10.0f * Time.deltaTime;
	}

	public bool IsGrounded()
	{
		return (mCollisionFlags & CollisionFlags.CollidedBelow) != 0;
	}
}
