﻿using UnityEngine;
using System.Collections;

public class MotorSeek : MotorSteer
{
	public float Force = 1.0f;
	public float BreakDist = 1.0f;
	
	Vector3 mMoveTo = Vector3.zero;
	bool mMoving = false;

	protected override void OnSteer()
	{
		base.OnSteer();
		
		if (mMoving)
		{
			SteerArival(mMoveTo, Force, BreakDist);
		}
	}
	
	//
	
	public void SetMoveTo(Vector3 moveTo)
	{
		mMoveTo = moveTo;
		mMoving = true;
	}
}
