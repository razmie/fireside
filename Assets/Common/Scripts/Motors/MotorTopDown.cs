using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
public class MotorTopDown : MonoBehaviour
{
	//	
	public float SpeedMax = 10.0f;
	public float Gravity = 50.0f;

	protected Vector3 mInpValue = Vector3.zero;
	protected Vector3 mInpDirection = Vector3.zero;
	public Vector3 InpDirection 		{get {return mInpDirection;}}
	protected bool mIsMoving = false;
	public bool IsMoving 				{get {return mIsMoving;}}
	
	protected void Awake()
	{
	}
	
	protected void Update()
	{
		//Get input values
		mInpValue.x = Input.GetAxisRaw("Horizontal");
		mInpValue.z = Input.GetAxisRaw("Vertical");
		mInpDirection = mInpValue.normalized;
		
		if (mInpValue.sqrMagnitude > 0.01f)
			mIsMoving = true;
		else
			mIsMoving = false;
	}
	
	protected void FixedUpdate()
	{
		Vector3 targetVelocity;
		targetVelocity = mInpDirection * SpeedMax;
	
		//Apply a force that attempts to reach our target velocity
		Vector3 velocity = GetComponent<Rigidbody>().velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		
		velocityChange.x = Mathf.Clamp(velocityChange.x, -SpeedMax, SpeedMax);
		velocityChange.z = Mathf.Clamp(velocityChange.z, -SpeedMax, SpeedMax);
		velocityChange.y = 0.0f;
		
		GetComponent<Rigidbody>().AddForce(velocityChange, ForceMode.VelocityChange);

		GetComponent<Rigidbody>().AddForce(new Vector3(0, -Gravity*GetComponent<Rigidbody>().mass, 0));
	}
}
