using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LookAtObject : MonoBehaviour 
{
	public GameObject TargetObj = null;
	public Vector3 OffsetAngle = Vector3.zero;

	protected void LateUpdate()
	{
		LookAtTarget();
	}
	
	//
	
	protected void LookAtTarget()
	{
		if (TargetObj)
		{
			Vector3 lookAtPos;
					
			//Set position
			lookAtPos = TargetObj.transform.position;
			
			//Look at target
			transform.LookAt(lookAtPos);
			
			if (OffsetAngle != Vector3.zero)
				transform.Rotate(OffsetAngle);
		}
	}
}