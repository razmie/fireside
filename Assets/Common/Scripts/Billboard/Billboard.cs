﻿using UnityEngine;
using System.Collections;
 
public class Billboard : MonoBehaviour
{
	public Camera MainCamera = null;
	
	void Update()
	{
		if (MainCamera == null)
			MainCamera = Camera.main;
			
		transform.LookAt(transform.position + MainCamera.transform.rotation * Vector3.forward,
		                 	MainCamera.transform.rotation * Vector3.up);
	}
}