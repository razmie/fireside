﻿using UnityEngine;
using System.Collections;

public class BillboardManager : MonoBehaviour
{
	public GameObject[] BillboardObjs = null;

	protected void Update()
	{
		if (BillboardObjs == null ||
		    BillboardObjs.Length == 0)
		    return;
		    
		for (int i = 0; i < BillboardObjs.Length; i++)
		{
			if (BillboardObjs[i])
				BillboardObjs[i].transform.LookAt(Camera.main.transform);
		}
	}
}
