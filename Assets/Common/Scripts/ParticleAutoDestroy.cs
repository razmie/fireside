﻿using UnityEngine;
using System.Collections;

public class ParticleAutoDestroy : MonoBehaviour
{
	public ParticleSystem[] ParticleSystems = null;

	protected void Awake()
	{
	}
	
	protected void Update()
	{
		bool isAlive = false;
		
		if (ParticleSystems != null &&
		    ParticleSystems.Length > 0)
		{
			for (int i = 0; i < ParticleSystems.Length; i++)
			{
				if (ParticleSystems[i] &&
				    ParticleSystems[i].IsAlive())
				{
					isAlive = true;
					break;
				}
			}
		}
		
		if (isAlive == false)
		{
			Destroy(gameObject);
		}
	}
}
