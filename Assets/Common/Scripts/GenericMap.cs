﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class GenericMap<TValue>
{
	[SerializeField]
	public List<string> Keys = new List<string>();
	
	[SerializeField]
	public List<TValue> Values = new List<TValue>();

	public virtual void Add(string key, TValue value)
	{
		if (Keys.Contains(key))
			return;
		
		Keys.Add(key);
		Values.Add(value);
	}
	
	public virtual void Remove(string key)
	{
		if (!Keys.Contains(key))
			return;
		
		int index = Keys.IndexOf(key);
		
		Keys.RemoveAt(index);
		Values.RemoveAt(index);
	}
	
	public virtual bool GetValue(string key, out TValue value)
	{
		//Default
		value = Values[0];
		
		if (!Keys.Contains(key))
			return false;
		
		int index = Keys.IndexOf(key);		
		value = Values[index];
		
		return true;
	}
	
	public virtual void ChangeValue(string key, TValue value)
	{
		if (!Keys.Contains(key))
			return;
		
		int index = Keys.IndexOf(key);
		Values[index] = value;
	}
	
//	[SerializeField]
//	public List<string> Keys = new List<string>();
//	
//	[SerializeField]
//	public List<GenericMapData> Values = new List<GenericMapData>();
//	
//	public virtual void Add(string key, GenericMapData value)
//	{
//		if (Keys.Contains(key))
//			return;
//		
//		Keys.Add(key);
//		Values.Add(value);
//	}
//	
//	public virtual void Remove(string key)
//	{
//		if (!Keys.Contains(key))
//			return;
//		
//		int index = Keys.IndexOf(key);
//		
//		Keys.RemoveAt(index);
//		Values.RemoveAt(index);
//	}
//	
//	public virtual GenericMapData GetValue(string key)
//	{
//		if (!Keys.Contains(key))
//			return null;
//		
//		int index = Keys.IndexOf(key);		
//		return Values[index];
//	}
//	
//	public virtual void ChangeValue(string key, GenericMapData value)
//	{
//		if (!Keys.Contains(key))
//			return;
//		
//		int index = Keys.IndexOf(key);
//		
//		Values[index] = value;
//	}
}