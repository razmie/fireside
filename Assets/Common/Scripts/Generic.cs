﻿using UnityEngine;
using System.Collections;

public class Generic : MonoBehaviour
{
	protected virtual void Awake()
	{
	}
	
	protected virtual void Start()
	{
	}
	
	protected virtual void Reset()
	{
	}
	
	protected virtual void Update()
	{
	}

	protected virtual void LateUpdate()
	{
	}
		
	protected virtual void FixedUpdate()
	{
	}
	
	protected virtual void OnDrawGizmos()
	{
	}
	
	protected virtual void OnDestroy()
	{
	}

	protected virtual void OnEnable()
	{
	}

	protected virtual void OnDisable()
	{
	}
		
	//
	
	public virtual void OnSphereCastHit(RaycastHit hit, int identifier)
	{
	}
}
