﻿using UnityEngine;
using System.Collections;

public class MouseRayHitter : MonoBehaviour
{
	//Editor
	public LayerMask LayerMask = 0;
	
	//
	private RaycastHit mHitInfo;
	public RaycastHit HitInfo		{get{return mHitInfo;}}
	
	private bool mIsHit = false;
	public bool IsHit					{get{return mIsHit;}}
	
	private Vector3 mHitPos = Vector3.zero;
	public Vector3 HitPos				{get{return mHitPos;}}
	
	void Update()
	{
		//Find collision
		if (Utils.ScreenPointToRaycastHit(Camera.main, LayerMask, out mHitInfo))
		{
			mIsHit = true;
			mHitPos = mHitInfo.point;
		}
		else
			mIsHit = false;
	}
	
	protected virtual void OnDrawGizmos()
	{
		if (Application.isPlaying == false)
			return;
		
		if (mIsHit)
		{
			Gizmos.color = Color.green;
	
			Gizmos.DrawSphere(mHitInfo.point, 0.125f);
		}
	}
}
