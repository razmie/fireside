﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Rigidbody))]

public class RigidBodySolver : MonoBehaviour 
{
	public float MinDistance = 0.2f;
	
	protected RaycastHit mSweepRayHit = new RaycastHit();
	protected Vector3 mPrevPosition = Vector3.zero;
	
	Vector3 posFrom;
	Vector3 posTo;
	
	protected void Awake() 
	{ 
		mPrevPosition = GetComponent<Rigidbody>().position;
	} 
	
	protected void FixedUpdate() 
	{
		posFrom = mPrevPosition;
		posTo = GetComponent<Rigidbody>().position;
		
		Vector3 diff = GetComponent<Rigidbody>().position - mPrevPosition;
		float distance = diff.magnitude;
				
		if (distance > MinDistance)
		{
			Vector3 direction = diff.normalized;
			//Vector3 newRigidBodyPos = rigidbody.position + diff;
			
			if (GetComponent<Rigidbody>().SweepTest(direction, out mSweepRayHit, distance))
			{
				//Kill Velocity
				GetComponent<Rigidbody>().velocity = Vector3.zero;
			}
		}

		mPrevPosition = GetComponent<Rigidbody>().position;
	}

	protected void OnDrawGizmos()
	{
		if (Application.isPlaying == false)
			return;
		
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(mSweepRayHit.point, 0.02f);
		Gizmos.DrawLine(posFrom, posTo);
	}
}